<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Edify_front</title>
    
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.fullPage.css">
    <link rel="stylesheet" type="text/css" href="assets/css/mystyle.css">
    
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- <script src="assets/js/gulpfile.js"></script> -->
    <script src="assets/js/jquery.fullPage.min.js"></script>
    <script>
        $(document).ready(function() { 
            $('#fullpage').fullpage({
	sectionsColor: ['#f2f2f2', '#4BBFC3', '#7BAABE', 'whitesmoke', '#000'],
    css3:true
});
        });
    </script>
   
</head>
<body>

<div id="fullpage">

 <!----first page starts here---->
 <div class="section">
    <div class="container-fluid bgimg">
        
            <div class="row rw1">
                <div class="col-lg-4">
                    <img src="assets/img/logo.png" class="imgs">
                </div>
                <div class="col-lg-8 lnk">
                    <span class=" col-lg-2"><a class="sp">Solutions</a></span>
                    <span class=" col-lg-2"><a class="sp">Portfolio</a></span>
                    <span class="col-lg-2"><a class="sp">Company</a></span>
                    <span class=" col-lg-1"><a class="sp">Blog</a></span>
                    <span class=" col-lg-1"><a class="sp">Contact</a></span>
                </div>
            </div>
            <div class="container">


            </div>
            <div class="row rw2 text-center">
                <div class="col-lg-4">
                    <img src="assets/img/icon-01.png">	
                </div>
                <div class="col-lg-4">
                    <img src="assets/img/icon-02.png" >
                </div>
                <div class="col-lg-4">
                    <img src="assets/img/icon-03.png">	
                </div>
            </div>
            <div class="row dta text-center">
                <div class="col-lg-4">
                    <h3>Clinical Data Management</h3>
                </div>
                <div class="col-lg-4">
                    <h3>Data Analysis & Big Data</h3>
                </div>
                <div class="col-lg-4">
                    <h3>Enterprise Mobility & Software</h3>
                </div>
            </div>
        </div>
    </div>

   <!---second page------>
   <div class="section">
    <div class="container-fluid">
              <div class="new row">
           
                    <div class="fnt1 container">
                      <p>ADORABLE</p> <p class="fnt2" >&</p><p> UNIQUE<span class="fnt3"> CONCEPT</span> </p>
                        <div class="row">
                            <div class="col-lg-4"><img src="assets/img/secondpage.jpg" alt="secondpage" style="height:200px;">
                            </div>
                              <div class="col-lg-4 txt1">Are you working in the Digital Marketing Analytics domain? Then you might have been challenged with the request: “May you let us know how the campaign A performed

                             </div>
                             <div class="triangle-bottomright"></div>
                        </div>
    
                    </div>
                    
                    
                </div>
            </div>
    
    </div>


</div>
 </body>
</html>
