<!DOCTYPE html>
<html>


<!doctype html>
<html lang="en">
<head>
	
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>img/apple-icon.png" />
  <link rel="icon" type="image/png" href="<?php echo base_url();?>img/favicon.png" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Edify</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="<?php echo base_url();?>assets/admin/css/material-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url();?>assets/admin/css/demo.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/admin/style.css" rel="stylesheet" />

	


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
		<style>

		</style>
</head>
<body>
<!-- <?php if(!isset($_COOKIE['uname'])){ redirect('admin/index');} ?> -->
	<div class="wrapper">

	    <div class="sidebar" data-color="purple" data-image="<?php echo base_url();?>/assets/admin/img/sidebar-1.jpg">
		

			<div class="logo">
				<a href="<?=base_url();?>" class="simple-text" target="_blank"><span class="fa fa-external-link"></span>
					Edify
				</a>
			</div>

	    	<div class="sidebar-wrapper">
	            <ul class="nav">
	               
	                <li>
	                    <a href="<?php echo base_url();?>/admin/changepassword">
	                        <i class="material-icons">person</i>
	                        <p>ChangePassword</p>
	                    </a>
	                </li>
	              <li>
	                    <a href="<?php echo base_url();?>/admin/view_portfolio">
	                        <i class="material-icons">receipt</i>
	                        <p>Portfolio</p>
	                    </a>
					</li>
					<li>
	                    <a href="<?php echo base_url();?>admin/view_blog">
						<i class="material-icons">library_books</i>
	                        <p>Blogs</p>
	                    </a>
					</li>
					<li class="active">
	                    <a href="<?php echo base_url();?>admin/view_message">
						<i class="material-icons">search</i>
	                        <p>Messages</p>
	                    </a>
                    </li>
                    <li>
	                    <a href="<?php echo base_url();?>/admin/logout">
	                        <i class="material-icons">person</i>
	                        <p>Logout</p>
	                    </a>
	                </li>
				
	            </ul>
	    	</div>
	    </div>

            