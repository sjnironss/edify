

<div class="content">
<div class="container-fluid" >
	<div class="row text-center">
		<div class="col-md-2"></div>
		<div class="col-md-10 cont" style="text-align:center;">
			<div class="card">
				<div class="card-header" data-background-color="purple">
					<h4 class="title">ADD PORTFOLIO</h4>
					<p class="category">Complete with Portfolioes..</p>
				</div>
				<div class="card-content">
					<form method="post" action="<?=base_url();?>Admin/add_portfolio_data" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group label-floating">
									<label class="control-label">Heading</label>
									<input type="text" class="form-control" name="heading" required >
								</div>
							</div>
						
			
							<div class="col-md-6">
								<div class="form-group label-floating">
									<label class="control-label">Long Heading</label>
									<input type="text" class="form-control" name="long_head" required >
								</div>
							</div>
						</div>

						<div class="row">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label class=""></label>
                                    <input type="file" class="form-control" name="image" style="opacity:.6" >
                                </div>
                            </div>
						</div>

						<div class="row">
							<div class="col-md-12">
							 <div class="form-group label-floating">
								 <label class="control-label">Description</label>
								 <textarea id="longd" name="desc">description here..</textarea>
							 </div>
							</div>
						</div>

						<button type="submit" id="upload" class="btn btn-primary pull-right">Submit</button>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
</div>




