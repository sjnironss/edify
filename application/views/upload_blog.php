 <?php
    $folder='uploads/images/';
    $orig_w=500;
   if(isset($_FILES['image']['tmp_name'])) {
        $imageFile=$_FILES['image']['tmp_name'];
        $filename=basename($_FILES['image']['name']);
        list($width,$height)=getimagesize($imageFile);
        $src=imagecreatefromjpeg($imageFile);
        $orig_h=($height/$width)*$orig_w;
        $tmp=imagecreatetruecolor($orig_w,$orig_h);
        imagecopyresampled($tmp,$src,0,0,0,0,$orig_w,$orig_h,$width,$height);
        imagejpeg($tmp,$folder.$filename,100);
        imagedestroy($tmp);
        imagedestroy($src);
        $filename=urlencode($filename);
        //header("Location:crop?filename=$filename&height=$orig_h");
       }
?> 
<html>
<head><title>jCrop Demo - Upload image</title></head>
<body>
<!-- <form action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" method="post">
<input name="image" type="file" value="image" /><br />
<br/>

<input type="submit" name="submit" value="uploadImage" /> -->
<!-- </form> -->

<div class="content">
<div class="container-fluid" >
	<div class="row text-center">
		<div class="col-md-2"></div>
		<div class="col-md-10 cont" style="text-align:center;">
			<div class="card">
				<div class="card-header" data-background-color="purple">
					<h4 class="title">ADD PORTFOLIO / BLOG</h4>
					<p class="category">Complete with Portfolio and Blog</p>
				</div>
				<div class="card-content">
					<form method="post" action="" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Short Heading</label>
									<input type="text" class="form-control" name="sheading" required >
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Long Heading</label>
									<input type="text" class="form-control" name="lheading" required >
								</div>
							</div>
						</div>

						<div class="row">

							<div class="col-md-6">
								<div class="form-group label-floating">
									<!-- <label class="control-label">Select Type</label> -->
									<!-- <input type="text" class="form-control" > -->
									<select class="form-control" name="type" required>
										<option value="">Select...</option>
									  <option value="portfolio">Portfolio</option>
									  <option value="blog">Blog</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group ">
									<label class=""></label>
                                    <!-- <a href="<?=base_url();?>Admin/upload">Image</a> -->
									<input type="file" class="form-control " name="content_image" style="opacity:.6"  id="fileUpload"  data-width="600" data-height="400">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
							 <div class="form-group label-floating">
								 <label class="control-label">Short Description</label>
								 <input type="text" class="form-control" name="shortd" required>
							 </div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
							 <div class="form-group label-floating">
								 <label class="control-label">Long Description</label>
								 <textarea id="longd" name="longd">long description here..</textarea>
							 </div>
							</div>
						</div>

						<button type="submit" id="upload" class="btn btn-primary pull-right" >Submit</button>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
</div>

</body>
<script type="text/javascript">
    $(document).ready(function() {
        //$("#fileUpload").on('change',function(){
           // alert('success');
            // $.ajax({
            //     type: 'POST',
            //     dataType: 'json',
            //     enctype: 'multipart/form-data',
            //     url: '<?php echo $_SERVER['PHP_SELF'];?>',
            //     async: false,
            //     data: {
                    
            //         'myfile': $('#fileUpload').val()
            //     },
            //     success: function(data) {
            //         alert(data.msg);
            //     }
            //});
       // });
       input.onclick = function () {
    this.value = null;
};

input.onchange = function () {
    alert(this.value);
};​
    });
</script>
</html>