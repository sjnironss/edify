
<div class="content">
<div class="container-fluid" >
	<div class="row text-center">
		<div class="col-md-2"></div>
		<div class="col-md-10 cont" style="text-align:center;">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                  <div class="row">
                    <div class="col-md-9" style="text-align:center">
                    <h4 class="title">PORTFOLIO/BLOG </h4>
                    <p class="category">Latest portfolio and blog...</p></div>

                </div> 
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                           <th>Sl.No</th>
                          <th>ShortHeading</th>
                          <th>LongHeading</th>
                          <th>Type</th>
                          <th>Content Image</th>
                          <th>ShortDescription</th>
                          
                          
                           <th>LongDescription</th>
                      
                        </thead>
                        <tbody>
                          <?php
                          $i=1;

            foreach ($result as $content) {?>

              <tr>
                  <td><?=$i?></td>
                  <td><?=$content['short_heading']?></td>

                  <td><?=$content['long_heading']?></td>
                 
                  <td><a><?=$content['type']?></a></td>
                  
                  <td>  <img src="<?php echo base_url().'/uploads/images/'.$content['content_image'];?>">
                 </td>
                  <td><?=$content['short_desc']?></td>
                 
                  <td><?=$content['long_description']?></td>
                   <?//php if($_COOKIE['type']=='admin'){ ?>
                  <td><a href="" data-toggle="modal" data-target="#myModal<?=$content['id'] ?>">Delete</a></td>
                  
                   
                       <div id="myModal<?=$content['id'] ?>" class="modal fade" role="dialog" style="z-index:10;">
                         <div class="modal-dialog">
                           <div class="modal-content">
                             <div class="modal-header">
                               <div class="modal-title">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h5>Do you want to delete ?</h5>
                                 </div>
                                 <div class="modal-footer">
                       <form method="post" action="<?=base_url();?>Admin/delete_content/<?=$content['id']?>">
                                               <button  class="btn btn-default" >
                                                   Yes</button>  </form>
                                               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                             
                                             </div>
                                           </div>
                                         </div></div>
                                         </div>
</div>
  <td><a href="" data-toggle="modal" data-target="#myModal2<?=$content['id'] ?>">Update</a></td>
                  
                   </tr>
                       <div id="myModal2<?=$content['id'] ?>" class="modal fade" role="dialog" style="z-index:10;">
                         <div class="modal-dialog">
                           <div class="modal-content">
                             <div class="modal-header">
                               <div class="modal-title">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h5>Do you want to update?</h5>
                                 </div>
                                 <div class="modal-footer">
                       <form method="post" action="<?=base_url();?>Admin/update_content/<?=$content['id']?>">
                                               <button  class="btn btn-default" >
                                                   Yes</button>  </form>
                                               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                             </div>
                                           </div>
                                         </div></div>
                                         </div>



             <?php $i++; }?>

                        </tbody>
                    </table>
                    </div>
                  </div>
                </div>
            </div>
        </div>


    </div>
