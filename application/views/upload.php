<?php
    $folder='uploads/images/';
    $orig_w=500;
    if(isset($_POST['submit'])){
        $imageFile=$_FILES['image']['tmp_name'];
        $filename=basename($_FILES['image']['name']);
        list($width,$height)=getimagesize($imageFile);
        $src=imagecreatefromjpeg($imageFile);
        $orig_h=($height/$width)*$orig_w;
        $tmp=imagecreatetruecolor($orig_w,$orig_h);
        imagecopyresampled($tmp,$src,0,0,0,0,$orig_w,$orig_h,$width,$height);
        imagejpeg($tmp,$folder.$filename,100);
        imagedestroy($tmp);
        imagedestroy($src);
        $filename=urlencode($filename);
        header("Location:crop?filename=$filename&height=$orig_h");
       }
?>

<html>
<head><title>jCrop Demo - Upload image</title></head>
<body>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" method="post">
<input name="image" type="file" value="image" /><br />
<br/>

<input type="submit" name="submit" value="uploadImage" />
</form>
</body>
</html>