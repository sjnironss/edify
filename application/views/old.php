<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Edify_front</title>
    
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.fullPage.css">
    <link rel="stylesheet" type="text/css" href="assets/css/mystyle.css">
    
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- <script src="assets/js/gulpfile.js"></script> -->
    <script src="assets/js/jquery.fullPage.min.js"></script>
    <script>
        $(document).ready(function() { 
            $('#fullpage').fullpage({
	sectionsColor: ['#f2f2f2', '#4BBFC3', '#7BAABE', 'whitesmoke', '#000'],
    css3:true
});
        });
    </script>
   
</head>
<body>

<div id="fullpage">
    <!----first page starts here---->
    <div class="section">
    <div class="container-fluid bgimg">
        
            <div class="row rw1">
                <div class="col-lg-4">
                    <img src="assets/img/logo.png" class="imgs">
                </div>
                <div class="col-lg-8 lnk">
                    <span class=" col-lg-2"><a class="sp">Solutions</a></span>
                    <span class=" col-lg-2"><a class="sp">Portfolio</a></span>
                    <span class="col-lg-2"><a class="sp">Company</a></span>
                    <span class=" col-lg-1"><a class="sp">Blog</a></span>
                    <span class=" col-lg-1"><a class="sp">Contact</a></span>
                </div>
            </div>
            <div class="container">


            </div>
            <div class="row rw2 text-center">
                <div class="col-lg-4">
                    <img src="assets/img/icon-01.png">	
                </div>
                <div class="col-lg-4">
                    <img src="assets/img/icon-02.png" >
                </div>
                <div class="col-lg-4">
                    <img src="assets/img/icon-03.png">	
                </div>
            </div>
            <div class="row dta text-center">
                <div class="col-lg-4">
                    <h3>Clinical Data Management</h3>
                </div>
                <div class="col-lg-4">
                    <h3>Data Analysis & Big Data</h3>
                </div>
                <div class="col-lg-4">
                    <h3>Enterprise Mobility & Software</h3>
                </div>
            </div>
        </div>
    </div>
    <!---second page------>
    <div class="section">
    <div class="container-fluid">
              <div class="new row">
           
                    <div class="fnt1 container">
                      <p>ADORABLE</p> <p class="fnt2" >&</p><p> UNIQUE<span class="fnt3"> CONCEPT</span> </p>
                        <div class="row">
                            <div class="col-lg-4"><img src="assets/img/secondpage.jpg" alt="secondpage" style="height:200px;">
                            </div>
                              <div class="col-lg-4 txt1">Are you working in the Digital Marketing Analytics domain? Then you might have been challenged with the request: “May you let us know how the campaign A performed

                             </div>
                             <div class="triangle-bottomright"></div>
                        </div>
    
                    </div>
                    
                    
                </div>
            </div>
    
    </div>
    <!---third page starts here------>
   
    <div class="section">
         <div class="container-fluid">
            <div class="new1 row  ">
                <div class="container">
                  <div class="row">
                      <div class="col-lg-5 font1">
                          WE HAVE <br>SOLUTIONS <br> FOR <br> EVERYONE
                      </div>
                          <div class="col-lg-6"><img src="assets/img/igor-son-328211.jpg" alt="secondpage" style="height:300px;width:370px;">
                              <p class="font2">We have worked on data from at least ten different industries during the last eleven years of our existence. From optimising your advertising spend to aggregating your MIS reports, we have several readymade solutions and frameworks that will help you quickly deploy a data analytics strategy. The M-Analytics paradigm that we have adopted often always help you make use of analytics frameworks on your mobile platforms and help you make decisions on the go. </p>
                         </div>
                     </div>
                    <div class="triangle-bottomleft"></div>
                  </div>
            </div>
        </div>
    </div>

    	<!--fourthpage-->
		<div class="section">
		<div class="container-fluid  thrd">
			<div class="container">
				<div id="triangle-topright">	
				</div>
				<div class="row rw3">
					<div class="col-lg-6 col1">
						<h1 class="h1">OUR</h1>
						<h1 class="h1">AMAZING</h1>
						<h1 class="h1">SKILLS</h1>
						<p class="para">This is simply a good</br> fact that it will describes</br> the total skills.</p>
					</div>
					<div class="col-lg-6">
						<div><img src="assets/img/nathan.jpg" class="img3"></div>
					
						<ul>
							<li>
							<div class="col-lg-2">R</div>
							<div class="col-lg-4">
								<div class="per1" style="width: 80%;"></div>
								<div class="per2"style="width: 20%;"></div>
							</div>
							<div class="col-lg-5">90%</div>
							</li>
							<li>
							<div class="col-lg-2">SOS</div>
							<div class="col-lg-4">
								<div class="per1" style="width: 65%;"></div>
								<div class="per2"style="width: 35%;"></div>
							</div>
							<div class="col-lg-5">92%</div>
							</li>
							<li>
							<div class="col-lg-2">Hadoop</div>
							<div class="col-lg-4 ">
								<div class="per1" style="width: 70%;"></div>
								<div class="per2"style="width: 30%;"></div>
							</div>
							<div class="col-lg-5">89%</div>
							</li>
							<li>
							<div class="col-lg-2">R</div>
							<div class="col-lg-4">
								<div class="per1" style="width: 90%;"></div>
								<div class="per2"style="width: 10%;"></div>
							</div>
							<div class="col-lg-5">95%</div>
							</li>
							<li>
							<div class="col-lg-2">SOS</div>
							<div class="col-lg-4">
								<div class="per1" style="width: 70%;"></div>
								<div class="per2"style="width: 30%;"></div>
							</div>
							<div class="col-lg-5">89%</div>
							</li>
							<li>
							<div class="col-lg-2">Hadoop</div>
							<div class="col-lg-4">
								<div class="per1" style="width: 90%;"></div>
								<div class="per2"style="width: 10%;"></div>
							</div>
							<div class="col-lg-5">95%</div>
							</li>
						</ul>
					</div>
				
			
				</div>
			
			</div>	
		</div>
		</div>
    <!----fifth page starts here-------->
<div class="section">
     <div class="container-fluid">
            <div class="new5 row  ">
                <div class="container">
                  <div class="row">
                          
                              <div class="font5">GROW WITH US</div>
                              <div><img src="assets/img/nicolas-picard-208276.jpg" alt="nicolas-picard" style="height: 275px; width: 340px; padding-top: 49px;">
                             </div>
                             <div class="paraph col-lg-8">Kreara comes up with analytics solutions that uses your data to your advantage. There are various engagement models that we work on depending on the size and volume of business that our customer does. For someone who need a continuous data analysis engagement we will have a dedicated team of statisticians and applied mathematicians churning your data and providing you with insights. If you have a specific business problem on the table that you would like us to take a shot at, then our functional consultants will work on your data. They will identify the sweet spots and come back to you with a specific analysis plan that could enhance your ROI. If you are not sure about what you want to do with your growing data, we will come in, structure and strategize your data analytics roadmap. In all these cases our sole intention will be to grow your business several fold using the power of data. Please let us take a look at your data and we will grow together.

                             </div>
                    
                         <div class="triangle-bottomright"></div>
                      
                  </div>
               </div>
         </div>
     </div>
</div>


<!--sixth page-->
<div class="section">
		<div class="container-fluid fourth">
			<div class="container ">
			<div class="triangle-bright"></div>
					<div class="row" style="margin-top:200px;">				
						<div class="col-lg-4 col2">
							<h1 class="h">OUR</h1><h1 class="h"> PORTFOLIO</h1>
							<p>This is simply a good</br> fact that it will describes</br> the total skills.</p>
						</div>
						<div class="col-lg-8" style="    padding-right: 144px;">
							<div class="row">
								<div class="col-lg-6">
									<img src="assets/img/aa.jpg" class="img4">
								</div>
								<div class="col-lg-6">
									<img src="assets/img/bb.jpg" class="img4">
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<img src="assets/img/nathan.jpg" class="img4">
								</div>
								<div class="col-lg-6">
									<img src="assets/img/cc.jpg" class="img4">
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
<!--------------7TH page starts ---------------->
<div class="section">
<div class="container-fluid">
       <div class="new7 row  ">
           <div class="container">
             <div class="row">
                     
                         <div class="font7">Our Clients</div>
                        <div  class="col-lg-12"> 
                            <div class="col-lg-7 para7">
                             Kreara serve clients of all scales and geographies. We have extensively worked in the US and European markets with specific focus on analytics and application solutions for Pharmaceutical, Financial and Retail Sectors. Recently we have also started to work with the government, helping them to analyse public data and visualise it in a very user friendly manner. We try and treat each of our clients as our only customer and work with them very closely to understand and cater to their requirements.
                            </div>
                             <div class="col-lg-5">
                                <div><img src="assets/img/Mercedes_Benz_logo.png" alt="benz" style="height: 100px;">
                                <img src="assets/img/puma-logo-png.png" alt="puma" style="height: 75px; padding-left: 51px;">
                                </div>
                                <div class = "pic1"><img src="assets/img/jaguar-logo.png" alt="jaguar" style="height: 80px;">
                                <img src="assets/img/mastercard-logo-png.png" alt="card" style="height: 50px; padding-left: 51px;">
                                </div>
                             </div>
                     </div>
                    <div class=" triangle-topleft"></div>
                  
                 </div>
             </div>
          </div>
    </div>
</div>

 <!--------------8TH page starts here---------------->  
 
<div class="section">
    <div class="container-fluid con">
            <div class="container cont">
                <div><h1>Get In Touch With Us</h1></div>
                <div><span>Fill Out The Form and we will get back to you asap.<span></div>
                <div><input type="text" name="name" placeholder="Name*" class="inpt"> </div>
				<div><input type="email" name="email" placeholder="Email*" class="inpt"></div>
                <div><input type="text" name="subject" placeholder="Subject" class="inpt"></div>
                <div><input type="text" name="message" placeholder="Your Message" class="inpt"></div>
                 <div class="row" style="margin-top: 20px;">
				 	<div class="col-lg-2">
					<span class="glyphicon glyphicon-map-marker"></span>
					 <span>INDIA</span>
					</div>
					<div class="col-lg-2">
					 <span class="glyphicon glyphicon-map-marker"></span>
					 <span>CANADA</span>
					</div>
					<div class="col-lg-2">
					 <span class="glyphicon glyphicon-map-marker"></span>
					 <span>CALIFORNIA</span>
					</div>
					<div class="col-lg-2">
					 <span class="glyphicon glyphicon-map-marker"></span>
					 <span>DUBAI</span>
					</div>
					<div class="col-lg-2">
					 <span class="glyphicon glyphicon-map-marker"></span>
					 <span>ABU DHABI</span>
					</div>
				 </div>  
				<div class="row frw">
					<div class="col-lg-6">
						<span class="glyphicon glyphicon-envelope  fa-lg"></span>
					 	<span>info@kreara.com</span>
					</div>
					<div class="col-lg-6">
						<span class="glyphicon glyphicon-earphone  fa-lg"></span>
					 	<span>914712527640</span>
					</div>
				</div> 
				<div class="row ffrw">
					<div class="col-lg-3">
					 <span class="fa fa-facebook fa-lg"></span>
					</div>
					<div class="col-lg-3">
					 <span class="fa fa-google-plus fa-lg" aria-hidden="true"></span>
					</div>
					<div class="col-lg-3">
					 <span class="fa fa-linkedin-square fa-lg"></span>
					</div>
					<div class="col-lg-3">
					 <span class="fa fa-twitter fa-lg"></span>
					</div>
				</div>
            </div>
            <div class="triangle-bleft"></div>
            
    	</div>
	</div>


   
}

</div> 
  </body>
</html>
