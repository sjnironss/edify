
<div class="content">
<div class="container-fluid" >
	<div class="row text-center">
		<div class="col-md-2"></div>
		<div class="col-md-10 cont" style="text-align:center;">
			<div class="card">
				<div class="card-header" data-background-color="purple">
					<h4 class="title">EDIT BLOG</h4>
					<p class="category">Complete with Blog</p>
				</div>
				<?php foreach ($result as $content) {?>
				<div class="card-content">
					<form method="post" action="<?=base_url();?>Admin/blog_update/<?=$content['blog_id']?>" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group label-floating">
									<label class="control-label">Name</label>
									<input type="text" class="form-control" name="name" value="<?=$content['name']?>" required >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group label-floating">
									<label class="control-label">Designation</label>
									<input type="text" class="form-control" name="designation" value="<?=$content['designation']?>" required >
								</div>
							</div>
						</div>

						<div class="row">

                            <div class="col-md-6">
								<div class="form-group ">
									<label class=""></label>
									<input type="file" class="form-control" name="image" value="<?$content['image']?>" style="opacity:.6" >
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
							 <div class="form-group label-floating">
								 <label class="control-label">Description</label>
								 <textarea id="longd" name="desc"> <?=$content['description']?></textarea>
							 </div>
							</div>
						</div>

						<button type="submit" class="btn btn-primary pull-right">Submit</button>
						<div class="clearfix"></div>
					<?php	}?>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
</div>
