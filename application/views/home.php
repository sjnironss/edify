<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title> Edify</title>
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico" type="image/x-icon" />
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css"> -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.fullPage.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/mystyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/animation.css">
	<script type="text/javascript">
		var portfolios = <?php echo json_encode($portfolio); ?>;
		var base_url = "<?php echo base_url(); ?>";
	</script>

</head>
<body class="loading">
<div id="main_loader" class="se-pre-con"></div>
	<div id="networkModal" class="modal md-effect-1" tabindex="-1" role="dialog">
		<div class="modal-dialog m-dialog" role="document">
			<div class="modal-content m-content">
				<div class="modal-header m-header">
					<h2 class="modal-title" id="hi" ></h2>
					<button type="button" class="close" id="modal_close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row network_content">
						<div class="modal_para col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<h3 class="text-left" id="head"></h3>
							<p id="desc" class="text-justify"></p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<img id="modal-image" height="150" class="img-responsive m_img img-thumbnail rounded float-right" alt="image">
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<nav class="navbar first_row navbar-fixed-top" id="menu">
        <div class="container-fluid">

            <div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myMenu">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
					<a href="#home"><img src="assets/img/logo.png" class="logo"></a>
			</div>

			<div class=" collapse navbar-collapse" id="myMenu">
            <ul class="nav navbar-nav navbar-right navs" >
				<li data-menuanchor="company" class=""><a href="#company">Company </a></li>
				<li data-menuanchor="solutions"><a href="#solutions">Solutions</a></li>
				<li data-menuanchor="blog"><a href="#blog">Blog</a></li>
				<li data-menuanchor="portfolio"><a href="#portfolio">Portfolio</a></li>
	   	      	<li data-menuanchor="contact"><a href="#contact">Contact</a></li>
            </ul>
			</div>

        </div>
	</nav>
<div id="fullpage">

	<div class="section first_section"  id="first_section" >

		<div id="particles-js"></div>
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-7 col-sm-6 col-xs-6 first_con">
					<div class="child ">
					<p class="heading">At edify, We Create Software Made With</p>
					<p class="first_para">
						We design innovative solutions with high end technologies that would simplifies IT complexities, helping business become more efficient and profitable anytime, anywhere.
          </p>
					</div>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-6 col-xs-6	img_parent">
					<div class="child">
						<img src="assets/img/landing_img.png" class="land img-responsive">
					</div>
				</div>
			</div>
			 <!-- <div class="row second_row">
				<div class="col-lg-7 col-md-7 col-sm-6 col-xs-6 first_con ">
					<p class="paraph heading">At edify,We Create Software Made With</p>
					<p class="first_para">
						We design innovative solutions with high end technologies that would simplifies IT complexities, helping business become more efficient and profitable anytime, anywhere.</p>


				</div>
				<div class="col-lg-5 col-md-5 col-sm-6 col-xs-6	">
				<img src="assets/img/landing-img.png" class="landing-img content_img">
				</div>
			</div>  -->
		</div>
	</div>
	<div class="section second-section text-center" data-anchor="company">
		<h2 class="exp_h">OUR EXPERTISE</h2>
		<p class="exp_para1">Its our Commitment to our customers that translates into projects delivered on time</p>
		<div class="container">
		 	<div class="owl-carousel" id="expertise-owl">

				<div class="item">
					<div class=" img_exp">
						<span class="span_icn fa fa-heartbeat fa-3x s "></span>
					</div>
					<div class="exp">
						<h5 class="text-center"><b>DATA DRIVEN CLINICAL RESEARCH</b></h5>
						<h5 class="exp_p">
						<?php $string ="<div><p>Edify Data Science offers comprehensive Clinical Data Management (CDM) services in adherence to regulatory and project specific requirements. Our CDM team comprises of highly skilled and experienced team members, who have worked with Phase I-IV trials and complex therapeutic areas. The CDM team works with the objective to deliver high quality data on time to achieve client’s expectations and to advance the process of regulatory submission. Our highly experienced team consistently achieves successful database locks with the help of 21 CFR part 11 compliant DM solutions, robust documentation system, Quality Audits at each stage of data processing and round-the-clock user support.
										</p></div> ";?>
						<?php $str1="	<div><p>We ensure your success with these technologies and services:</p>
										</div>";?>
						<?php $str2="<div><ul><li>Case Report Form Designing (CRF / eCRF)</li></ul>
									<ul><li>Database Build and Designing</li></ul>
									<ul><li>Validation Programming (Simple / Advanced)</li></ul>
									<ul><li>User Acceptance Testing</li></ul>
									<ul><li>Data Capture</li></ul>
									<ul><li>Electronic Data Capture in EDC studies</li></ul>
									<ul><li>Double Data Entry in Paper studies</li></ul>
									<ul><li>Discrepancy Management</li></ul>
									<ul><li>Manual Review</li></ul>
									<ul><li>System Review</li></ul>
									<ul><li>Query Management</li></ul>
									<ul><li>Self Evident Corrections</li></ul></div>";?>
						<?php $str3="<div><ul><li>Tracking & Closing Queries</li></ul>
									<ul><li>SAE Reconciliation</li></ul>
									<ul><li>External Data Handling (Lab, Device Data etc.)</li></ul>
									</div>

									<ul><li>Medical Coding</li></ul>
									<ul><li>MedDRA for Adverse Events</li></ul>
									<ul><li>WHO DD for Concomitant Medications</li></ul>
									<ul><li>Quality Review</li></ul>
									<ul><li>Initial, Ongoing, Final Quality Review</li></ul>
									<ul><li>Quality Review for Critical Parameters (100%)</li></ul>
									<ul><li>Quality Assurance</li>
										<ul><li>Independent review for each document</li>
											<li>Involved at each stage of data processing</li>
										</ul>
									</ul>
									<ul><li>Database lock activities</li>
										<ul><li>Database close (Soft lock)</li>
											<li>Database lock (Hard lock)</li>
										</ul>
									</ul></div>";?>
						<?php $str4="<div>
									<p>The Clinical Data Management team at Edify has extensive experience in paper as well as in EDC study across all therapeutic areas namely, Oncology, Dermatology, Ophthalmology, Analgesic, Gastroenterology, Reproductive System, Hypertension and Endocrinology and others.</p>
									";?>
						<?php $str5="<h5><b>BIOSTATISTICS</b></h5>";?>
						<?php $str6="<p>Edify Data Science has experienced and dedicated team of Biostatisticians, Statistical SAS programmers, Report writers and Quality Assurance personnel. Our expertise includes detailed approach in performing analysis and deriving interpretation. All the procedures are in accordance with well-defined documentation, check-list and SOPs, which leads to robust results for the trials.
									</p>
									<p>
									We are equipped with a strong team of Biostatisticians & SAS programmers with extensive experience in statistical analysis and SAS coding across therapeutic areas of Oncology, Dermatology, Ophthalmology, Analgesic, Gastroenterology.
									</p>
									<p>
									Our Biostatisticians are well trained in ICHe9 and applicable regulatory guidelines. We are experienced in implementing high standards for SAS programming. Our Biostatisticians provide effective inputs for clinical trials study design and sample size estimation.
									</p>";?>
						<?php $str7="<p>Statistical services:</p>
									<ul><li>Clinical trial study design</li></ul>
									<ul><li>Sample Size calculation</li></ul>
									<ul><li>Randomization</li></ul>
									<ul><li>Statistical Analysis Plan</li></ul>
									<ul><li>Mock TFLs</li></ul>
									<ul><li>SAS programming for TFL's</li></ul>
								<ul><li>CDISC standards- SDTM and ADaM compliant datasets</li></ul>
								<ul><li>Statistical Report writing</li></ul>
								<ul><li>Support for the further research</li></ul>
								";?>
								<?php $str8="<h5><b>MEDICAL WRITING</b></h5>";?>
								<?php $str9="<p>Our medical writing team comprises qualified medical writers with deep scientific domain expertise in CSR and Protocol writing.

								<p>We have strong proficiency across the end-to-end medical writing portfolio including Protocols, CRFs, ICFs, CSRs etc. wherein we have standard ICHE3, ICHE6 and Schedule Y based templates, CRF libraries of standard panels to provide rapid turnaround.
								</p>
								<p>Edify medical writing team is involved in :
								</p>
								<ul><li>Protocol synopsis development</li></ul>
								<ul><li>Protocol design</li></ul>
								<ul><li>ICF design</li></ul>
								<ul><li>CRF design</li></ul>
								<ul><li>CSR development</li></ul>
								<ul><li>Final report compilation in eCTD format</li></ul>
								";?>
								<?php $str10="<h5><b>CDISC SERVICES [SDTM & ADaM]</b></h5>";?>
								<?php $str11="<p>
								Edify makes you profitable in terms of standard by converting your valuable data into e-submissions ready package.
								</p>
								<p>CDISC Services

								</p>
								<ul><li>Consulting for CDISC Standards</li></ul>
								<ul><li>SDTM conversion from Specifications to Define files</li></ul>
								<ul><li>ADaM Implementation</li></ul>
								<ul><li>Customized CDISC solutions</li></ul>
								<ul><li>Compatibility with any version of the SDTM Implementation Guidet</li></ul>
								<ul><li>Statistical programming using ADaM derived datasets</li></ul>
								<ul><li>SDTM & ADaM Packages Review & Validation</li></ul>
								<p> Regulatory authorities require data in standard format which is implemented over the globe. Clinical Data Interchange Standards Consortium (CDISC) has developed the various standards which include required standards Study Data Tabulation Model (SDTM) and Analysis Data Model (ADaM). FDA has been already endorsing and reviewing CDISC standards since long back. FDA & PMDA has been already implemented CDISC standards and other regulatory authorities are also in process of the same. The data standards submission is becoming more important and challenging as FDA’s latest published guideline on December 2014 indicates that submission has to be done in CDISC standards for trials started after December 2016.</p>
								";?>
								<?php $str14="<h5> How can Edify Data Sciences help?</h5>";?>
								<?php $str15="<p> We can successfully convert the unstructured data into standard format with our proprietary process and accelerators. Our processes enable faster, efficient and reliable conversion of existing clinical trial data to standard domains that would be part of the submission to the regulatory authorities for their review and approval. </p>
								<p>Our conversation process is powered by standard operating procedures (SOP) which embed best practices of CRF annotation, logical mapping, validating datasets by tool & double programming efforts, report generations and guidance documents for all the activities.</p>
								";?>
								<?php $str12="<h5>Our Expertise </h5>";?>
								<?php $str13="<ul><li>Define XML</li></ul>
								<ul><li>Designing company-specific CDISC Standards</li></ul>
								<ul><li>Extensive ODM-XML knowledge</li></ul>
								<ul><li>Knowledge  of the therapeutic area</li></ul>
								<ul><li>Extensive experience in CDISC SDTM and ADaM conversions and standards</li></ul>
								</div>";
                  if (strlen($string) > 150) {
                      $trimstring = substr($string, 0, 150). '.. <a href="#" role="button" class="read_more" data-toggle="modal" data-target="#cdmModal">Read more.</a>';
                  } else {
                      $trimstring = $string;
                  }
                  echo $trimstring;
                  ?>
						</h5>
					</div>

				</div>
				<div class="item">
					<div class="img_exp">
					<span class="span_icn fa fa-database fa-3x"></span>

					</div>
					<div class="exp">
						<h5 class="text-center"><b>DATA SCIENCE AS A SERVICE</b></h5>
						<h5 class="exp_p">
						<?php $data= "Forward-thinking businesses today have a gem of opportunities with the data that is generated across various sources within and outside enterprise. The importance of data science as a service is now becoming requisite for businesses to extract sheer values out of the data.So they prefer to collaborate with expert data science consulting and business analytics solutions partners. Choosing expert partners allows them to gain abilities that help effectively reduce revenue leakages, identify frauds, heighten revenue curve, augment sales conversions, and improve customer experience.
											<p>Our Data science as a service helps enterprises make informed data-driven business decisions and find innovative ways to strategize and optimize operations, while discovering new market opportunities and gaining the above benefits.</p>
											";?>
						<?php $data1="<h4><b>Our Offerings</b></h4>";?>
						<?php $data2="<h5><b>Data Preparation and Ingestion</b></h5>";?>
						<?php $data3="<p>Edify uses cutting-edge tools for data preparation, management and harmonization. Our data preparation method comprises of:</p>
											<ul><li>Data exploration</li></ul>
											<ul><li>Data cleaning</li></ul>
											<ul><li>Data changing</li></ul>
											<ul><li>Data shaping</li></ul>
											<ul><li>Data publishing for analysis</li></ul>
											<p>Our data ingestion service ensures that data is gathered, imported, transferred, loaded, and processed flawlessly for immediate use or storing in a database for later use.</p>
											";?>
						<?php $data4= "<p><b>Statistical Modeling and Algorithm Development</b></p>
											";?>
						<?php $data5= "<p>We leverage statistical methodologies, machine learning or a blend of both these prevalent practices to create analytical models on big data. The statistical modelling service spans:</p>
											<ul><li>Data analysis</li></ul>
											<ul><li>Data interpretation</li></ul>
											<ul><li>Data explanation</li></ul>
											<ul><li>Data presentation</li></ul>";?>
						<?php $data6= "<h5><b>Insight Generation</b></h5>";?>
						<?php $data7="<p>We take industry-standard approach to insight generation. The process includes gathering, organizing and retaining data, generating various analytics models from the data, and analyzing the analytics models for building actionable business strategies. Insight generation covers:</p>

											<ul><li>Data mining</li></ul>
											<ul><li>Model building</li></ul>
											<ul><li>Business insights</li></ul>";?>
						<?php $data8= "<h5><b>Insight Deployment</b></h5>";?>
						<?php $data9="<p>Our insight deployment process focuses on applying the analytical results into the routine decision making process and automate it.</p>
											<ul><li>Write back of insights to current processes and systems</li></ul>
											<ul><li>Gathering results of analytics implementation through feedback loop</li></ul>
											<ul><li>Advancement of analytics processes according to feedback loop</li></ul>
											<ul><li>Predictive processes and systems’ automation using analytics-driven insights</li></ul>
											";?>
						<?php $data10= "<h5><b>Tools we have expertise In</b></h5>";?>
						<?php $data11= "<ul><li>R</li></ul>
											<ul><li>Python</li></ul>
											<ul><li>Sas</li></ul>
											<ul><li>Hadoop</li></ul>
											<ul><li>Spark</li></ul>
											<ul><li>AdvancedMiner</li></ul>
											<ul><li>Tableau</li></ul>
											";
                            if (strlen($data) > 150) {
                                $trimstring = substr($data, 0, 150). '.. <a href="#" role="button" class="read_more" data-toggle="modal" data-target="#dsModal">Read more.</a>';
                            } else {
                                $trimstring = $data;
                            }
                            echo $trimstring;
                            ?>

						</h5>
					</div>
				</div>
				<div class="item">
					<div class="img_exp">
						<!-- <img src="assets/img/Big-Data-Analytics.png"  class="img-responsive img_exp"> -->
						<span class="span_icn fa fa-line-chart "></span>

					</div>
					<div class="exp">
						<h5 class="text-center"><b>BIG DATA ANALYTICS</b></h5>
						<h5 class="exp_p">
						<?php $bdata = "The goal of taking customer experience to the next level remains a hard-hitting challenge for many organizations across various verticals across the globe. Customers are expecting personalized services that deeply understand their requirement and help them get done things without leaving any room for hassle. But a hurried move by enterprises towards customer-centric approach and its one of the consequences “difficulty to manage mounting up customer data” is posing difficulties for them to delivering improved customer experience across all channels and creating value across customer experience life cycle. This is where our Big Data Analytics and Big Data solutions help businesses build streamlined customer experience life cycle.
										<p>Our Big Data and Analytics solutions don’t just focus on customer experience life cycle, but also help enterprises get insights into market trends, discover new markets, generate new revenue streams, uncover hidden and unknown correlations, improve enterprise-wide performance, and get other strategy-oriented information that can help them in business intelligence and make more informed business decisions. We leverage our deep industry experience and expertise in data analytics and our own sought-after data analytics products and other most sought-after Big Data Analytics tools and technologies in the market like NoSQL and Hadoop to create result-oriented Big Data Analytics solutions for your enterprise.</p>
										";?>
						<?php $bdata1 = "<h5><b>Key Benefits of Our Big Data Analytics Solutions</b></h5>
										";?>
						<?php $bdata2 = "<p><b>Enhance customer life cycle value:</b>Our BDA solutions help you leverage customer data on purchase preferences, purchase patterns, wish lists, feedback, enquiries etc. to draw customer insights in real time. It helps you leverage the following deeper benefits:</p>
										<ul><li>Boost customer retention rate</li></ul>
										<ul><li>Achieve a seamless customer experience across channels, products and lifecycle</li></ul>
										<ul><li>Boost up profitability through cross/up sell</li></ul>
										<ul><li>Develop new products with insights into the performance of existing products, market reach, and demand</li></ul>
										<ul><li>Launch fruitful loyalty programs</li></ul>
										";?>
						<?php $bdata3 = "<p><b>Achieve improved operational efficiency:</b> Our Big Data Analytics solutions can help you discover ways to make operations more effective and efficient by bettering asset efficiency and streamlining operations at multiple locations.</p>
										<p><b>Achieve competitive advantage:</b> The 63 percent of respondents who were brought into a survey recently believe that big data and analytics solutions are creating a competitive advantage for their organizations. Our BDA solutions can help you achieve a competitive advantage by improving your decision-making capability, minimizing risks, unearthing valuable insights and developing next generation products and services with profound insights into existing markets and customer behaviour.</p>
										<p><b>Create new revenue opportunities:</b> With the ability to track and analyze most customer interactions and transactions across devices and channels, our Big Data Analytics solutions help you transform raw enterprise data into actionable insights and create new revenue streams across various markets. </p>
										";?>
						<?php $bdata4 = "<h5><b>Key Qualities of Our Big Data Analytics Solutions</b></h5>
										";?>
						<?php $bdata5= "<p>Our Big Data Analytics solutions encompass the following approaches and qualities to be unique in the market and be unique for our customers.</p>
										<ul><li>Exploratory Data Analysis and Data Discovery approaches</li></ul>
										<ul><li>Cloud-based Integrated Data Analytics Platform</li></ul>
										<ul><li>Machine learning mechanism based builds that well understand the behavioral aspects of decision making</li></ul>
										<ul><li>Analytical information on real-time activities and predictive models for more informed decision making</li></ul>
										<ul><li>Future-compatible Big Data Analytics architecture</li></ul>
										";
                            if (strlen($bdata) > 100) {
                                $trimstring = substr($bdata, 0, 150). '.. <a href="#" role="button" class="read_more" data-toggle="modal" data-target="#bdModal">Read more.</a>';
                            } else {
                                $trimstring = $bdata;
                            }
                            echo $trimstring;
                            ?>
						</h5>
					</div>

				</div>
				<div class="item">
					<div class="img_exp">
						<!-- <img src="assets/img/predictive-analytics.png"  class="img-responsive img_exp"> -->
						<span class="span_icn fa fa-lightbulb-o fa-3x "></span>
					</div>
					<div class="exp">
						<h5 class="text-center"><b>PREDICTIVE MODELLING</b></h5>
						<h5 class="exp_p">
						<?php $predictive = "As a society, we are accumulating data on an exponential scale. IBM reports that 90 percent of the data available today was created just in the past two years. Once companies are logging and storing detailed data on all their customer engagements and internal processes, what’s next? Presumably, firms are investing in big data infrastructure because they believe that it offers a positive return on investment. However, looking at the surveys and consulting reports, it is unclear what the precise use cases are that will drive this positive ROI from big data.
											";?>
						<?php $predictive3 = "<p>Fortunately, many predictive modeling techniques, including neural networks (NNs), clustering, support vector machines (SVMs), and association rules, exist to help translate this data into insight and value. They do that by learning patterns hidden in large volumes of historical data. Today, predictive analytics permeates our everyday lives, from fraud detection in financial transactions (every time you use your credit card to buy something at a store or online, it is analyzed for its fraud potential) to marketing and recommender systems.<p>
											";?>
						<?php $predictive4 = "<p>From predicting demand to developing radically new applications predictive modeling is being used to improve business across industries.</p>
											";?>
						<?php $predictive5 = "<p>Our experts and data scientists can help you better understand the potential of Predictive modelling for your business.</p>
											";?>
						<?php $predictive1 = "<p>Industries we serve</p>";?>
						<?php $predictive2 = "<ul><li>Retail</li></ul>
											<ul><li>Banking and Finance</li></ul>
											<ul><li>E-Commerce</li></ul>
											<ul><li>Healthcare</li></ul>
											<ul><li>Telecommunication</li></ul>

											";
                            if (strlen($predictive) > 100) {
                                $trimstring = substr($predictive, 0, 150). '.. <a href="#" role="button" class="read_more" data-toggle="modal" data-target="#pmModal">Read more.</a>';
                            } else {
                                $trimstring = $predictive;
                            }
                            echo $trimstring;
                            ?>


						</h5>
					</div>

				</div>
				<div class="item">
					<div class="img_exp">
						<span class="span_icn fa fa-cogs fa-3x"></span>
						<!-- <img src="assets/img/machine-leaning.png"  class="img-responsive  img_exp"> -->
					</div>
					<div class="exp">
						<h5 class="text-center"><b>MACHINE LEARNING</b></h5>
						<h5 class="exp_p">
						<?php $machine = "For decades Machine Learning has been a field dominated by scientists and the few organizations with enough computing power to run complex algorithms against huge datasets. But now the world of machine learning and predictive analytics is opening up to developers and companies of all sizes, with machine learning (ML) providers offering their products through a subscription-based model or open sourcing some of their technology.

									<p>Edify leverages the full potential of pattern recognition, mathematical optimization, computational learning theory, self-optimizing, and nature-inspired algorithms to the fullest advantage of its customers, providing tailor-made machine learning services and solutions.	</p>
									";?>
								<?php $machine1 ="<p>Our solutions will help you in the following areas of business operations:</p>
									";?>

									<?php $machine2 ="	<h5><b>Sales</b></h5>";?>
									<?php $machine3 =" <p>It is important for sales and account managers to get automatic alerts from the algorithms about specific customers who are about to turn away. Our ML solutions offer you the following benefits:</p>
									<ul><li><b>Improve sales forecasting:</b> Automatically compare the data of new leads, such as company size, stakeholders, and solutions they want, to historical sales efforts. Thus you can better predict what solutions would be effective for the new client and forecast the time taken to close the deal, and accordingly, you can allocate resources and predict sales projections.</li></ul>
									<ul><li><b>Interpret customer data:</b> Make the sense of the customers’ data that you collect about your customers.</li></ul>
									<ul><li><b>Predict customer needs: </b>Address your clients’ needs before they get escalated and suggest a solution as well</li></ul>
									<ul><li><b>Efficient transactional sales:</b> Handle certain transactional tasks quickly and effectively</li></ul>
									<ul><li><b>Sales communication:</b> Quickly and easily answer queries about pricing, product features or contract terms</li></ul>
									";?>
										<?php $machine4 ="<h5><b>Marketing</b></h5>";?>
										<?php $machine5 ="<p>Personalizing marketing campaigns is significant to understand the exact needs of prospective customers. You can offer customers special offers based on their previous buying patterns.</p>
									";?>
										<?php $machine6 ="<h5><b>Human Resources</b></h5>";?>
										<?php $machine7 ="<p>Application tracking & assessment, attracting fine talents, behavior tracking, attrition detection, and individual skills management/performance development are some of the areas in HR that will benefit from our Machine learning solutions.</p>
									";?>
										<?php $machine8 ="<h5><b>Finance</b></h5>";?>
										<?php $machine9 ="<p>Hidden opportunities in Finance can be unearthed using Machine learning techniques in Fraud detection, loan/insurance underwriting, algorithmic trading, portfolio management, sentiment/news analysis, and customer service </p>
									";?>
										<?php $machine10 ="<h5><b>Healthcare</b></h5>";?>
										<?php $machine11 ="<p>Algorithms that learn to recognize complex patterns within rich and massive data are to be developed. Our team's focus lies in developing and applying machine learning and data mining tools to an array of different challenging problems from clinical genomic analysis, through designing clinical decision support systems, to analyzing real world evidence for personalized medicine.</p>

									";
                            if (strlen($machine) > 100) {
                                $trimstring = substr($machine, 0, 150). '.. <a href="#" role="button" class="read_more" data-toggle="modal" data-target="#mlModal">Read more.</a>';
                            } else {
                                $trimstring = $machine;
                            }
                            echo $trimstring;
                            ?>
						</h5>
					</div>

				</div>
				<div class="item">
					<div class="img_exp">
						<span class="span_icn fa fa-binoculars fa-3x"></span>


					</div>
					<div class="exp">
						<h5 class="text-center"><b> DEEP LEARNING</b></h5>
						<h5 class="exp_p">
						<?php $deep1 = "Deep Learning is a new area of Machine Learning research, which has been introduced with the objective of moving Machine Learning closer to one of its original goals: Artificial Intelligence. More than 19,000 companies are currently using deep learning to improve their products and services, solving what was once unsolvable.
										";?>
										<?php $deep2= "
										Deep learning is becoming a driving force behind solving challenges in areas such as:
										<ul><li>Demand prediction</li></ul>
										<ul><li>Medical diagnosis</li></ul>
										<ul><li>Anomaly detection</li></ul>
										<ul><li>Customer churn</li></ul>
										<ul><li>Fraud detection</li></ul>
										<ul><li>Image recognition</li></ul>
										<ul><li>Generative modeling</li></ul>
										<ul><li>Signal classification</li></ul>
										<ul><li>Speech recognition</li></ul>
										<ul><li>Natural Language processing</li></ul>
										";?>
									<?php $deep3= "	<p>At Edify, we offer cutting-edge services and solutions on deep learning to solve your business challenges in the above areas and to draw innovative benefits in those areas. Our custom-built deep learning solutions help you build powerful and intelligent artificial neural network models that are efficient enough to automatically learn complex representations of data.</p>
									";?>
									<?php $deep4= "<h5><b>Our offerings</b></h5>";?>
									<?php $deep5= "<ul><li>Natural Language Processing </li></ul>
									<ul><li>Image Recognition</li></ul>
									<ul><li>Speech Recognition and more</li></ul>

									";
                            if (strlen($deep1) > 100) {
                                $trimstring = substr($deep1, 0, 150). '.. <a href="#" role="button" class="read_more" data-toggle="modal" data-target="#dlModal">Read more.</a>';
                            } else {
                                $trimstring = $deep1;
                            }
                            echo $trimstring;
                            ?>
						</h5>
					</div>

				</div>
				<div class="item">
					<div class="img_exp">
						<span class="span_icn fa fa-keyboard-o"></span>
					</div>
					<div class="exp">
						<h5 class="text-center"><b>ENTERPRISE MOBILITY AND SOFTWARE</b></h5>
						<h5 class="exp_p">
						<?php $ems1 = "<p>Our mobility solutions combine the latest in mobile technology, methods of engagement, and system integration — to deliver results that matters. We offer a complete spectrum of services — such as mobile application development and maintenance, mobile testing services, and industry-focused mobility solutions. At Edify, we will help you create new efficiencies with mobile-driven business processes. Our app developers deliver enterprise applications that address your business needs and meet your specifications. </p>
										<p>We have experience in creating software that improves content management, business process automation, e-Commerce, and employee collaboration. Our developers are able to leverage their experience with Agile software development to increase your competitive advantage. Edify provides IT consulting and transformative business services that enable large enterprises to harness more value and efficiency from their value chain. We have become known for delivering enterprise IT consulting leadership that tackles complex challenges with effective, flexible and scalable solutions. </p>
										";?>
						<?php $ems2= "	<p>Our specializations include:</p>
										<ul><li>Application Development & Management</li></ul>
										<ul><li>ERP Solutions and Integration</li></ul>
										<ul><li>Cloud Services</li></ul>
										<ul><li>Core Business Operations Consulting</li></ul>
										<ul><li>Information Management Services</li></ul>
										<ul><li>IT Consulting</li></ul>
										<ul><li>Outsourcing</li></ul>
										<ul><li>Scalable Implementation</li></ul>
										<ul><li>Systems Migration and Integration</li></ul>
										<ul><li>Technical Support</li></ul>

									";
                            if (strlen($ems1) > 100) {
                                $trimstring = substr($ems1, 0, 150). '.. <a href="#" role="button" class="read_more" data-toggle="modal" data-target="#emsModal">Read more.</a>';
                            } else {
                                $trimstring = $ems1;
                            }
                            echo $trimstring;
                            ?>
						</h5>
					</div>

				</div>

			</div>
		</div>
	</div>
	<div class="section third-section" data-anchor="solution">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<img src="assets/img/solutions.png" class="img-responsive content_img">
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 concept" >
					<p ><h3 class="third_para1" >WE HAVE SOLUTIONS FOR EVERYONE</h3></p>
					<p class="third_para2">With 10+ years experience in the field of web and mobile application development and over 25 projects successfully delivered, you can count on us to get your project done, irrespective of your industry. </p>
				</div>
			</div>
		</div>

	</div>
	<!-- <div class="section fourth-section" data-anchor="solution">
		<div class="container">
			<div class="row">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 solution_icon">
				<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon1">
					<img src="assets/img/icon-5.png" class="img-responsive ">
				</div>
				</div>
				<div class="row" style="padding-top:20px;">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 icon2">
					<img src="assets/img/icon-3.png" class="img-responsive">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 icon3" >
					<img src="assets/img/icon-4.png" class="img-responsive ">
				</div>
				</div>
				<div class="row" style="padding-top:20px;">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 icon4">
					<img src="assets/img/icon-1.png" class="img-responsive">
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 icon5">
					<img src="assets/img/icon-5.png" class="img-responsive ">
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 icon6">
					<img src="assets/img/icon-6.png" class="img-responsive ">
				</div>
				</div>
				<div class="row" style="padding-top:20px;">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 icon7">
					<img src="assets/img/icon-2.png" class="img-responsive">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 icon8">
					<img src="assets/img/icon-3.png" class="img-responsive ">
				</div>
				</div>
				<div class="row" style="padding-top:20px;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon9">
					<img src="assets/img/icon-4.png" class="img-responsive">
				</div>
				</div>

				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<h3 class="solution_para">WE HAVE SOLUTIONS FOR EVERYONE</h3>
					<p class="fourth_para2">With 10+ years experience in the field of web and mobile application development and over 25 projects successfully delivered, you can count on us to get your project done, irrespective of your industry.</p>
				</div>
			</div>
		</div>
	</div> -->
	<div class="section blog-section" data-anchor="blog">

			<div class="blog">
				<h2>OUR BLOG</h2>
			</div>


				<div class="owl-carousel owl-theme" id="blog-owl">
					<?php  foreach ($blog as  $blog) {
                                ?>
					<div class="item text-center" style="padding-top:30px;">
						<div class="img_blog ">
							<img src="<?php echo base_url(); ?>uploads/images/<?php echo $blog['image']; ?>" class="img-circle  img-responsive con_img">
						</div>
						<div class="">
							<h4><?php echo $blog['name']; ?></h4>
							<h5><?php echo $blog['designation']; ?></h5>
						</div>
						<div class="contnt">
							<p class=""><?php echo $blog['description']; ?></p>
						</div>
					</div>
					<?php
                            } ?>
				</div>





	</div>
	<div class="section portfolio" data-anchor="portfolio">
		<div>
			<h2 class="portfolio_head">OUR PORTFOLIO</h2>

			<p class="portfolio_para">Lorem Ipsum is simply dummy text of the printing and typesetting industry text of the printing.</p>
		</div>
				<div class="bubble-holder">
					<div id="bubbles"></div>
				</div>
		</div>
	<div class="section graph-section">
		<div class="container">
			<div class="row sixth_row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 box" >
					<div class="img_group ">
						<img src="assets/img/003-group.svg">
					</div>
					<div class="text-center line1">

					<span class="counter" data-count="200">0</span><span>+</span>
					</div>
					<div class="text-center">
						<span class="num">Clientele</span>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 box" >
					<div class="text-center img6">
						<img src="assets/img/001-route.svg">
					</div>
					<div class="text-center line">
					<span class="counter" data-count="300">0</span><span>+</span>
					</div>
					<div class="text-center">
						<span class="num">Projects Executed</span>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" >
					<div class="text-center img6">
						<img src="assets/img/002-star.svg">
					</div>
					<div class="text-center line">
					<span class="counter" data-count="100">0</span><span>%</span>
					</div>
					<div class="text-center">
						<span class="num">Customer Retention</span>
					</div>
				</div>



			</div>
		</div>

	</div>
	<div class="section seventh-section" data-anchor="contact">
		<!-- <div class="container cont">
			<div class=" h_contact"><p class=" heads">Get In Touch With Us</p>
				<h5 class="hd">Fill Out The Form and we will get back to you asap.</h5>
			</div>
			<form method="post" action="<?php echo base_url();?>Admin/send_message">
					<div class="">
						<div class="" style="padding-top:20px;"><input type="text" name="name" placeholder="Name*" class="inpt" >
				</div>
				<div class=""><input type="email" name="email" placeholder="Email*" class="inpt"></div>
				<div class=""><input type="text" name="subject" placeholder="Subject" class="inpt"></div>
				<div class=""><textarea type="text" name="message" placeholder="Your Message" class="texta"></textarea></div>
				<div class="btn"><input type="submit" name="submit" value="submit" class="send"></div>
			</div>
			</form>
			<div class="row frw">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
						<span class="glyphicon glyphicon-envelope  fa-lg"></span>
						<span class="contact">edify.com</span>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6  text-center">
						<span class="glyphicon glyphicon-earphone  fa-lg"></span>
						<span class="contact">914712527640</span>
					</div>
				</div>
			</div>
			<ul class="text-center ffrw" >
				<li class="fa fa-facebook fa-1x " style="margin-right:20px;"></li>
				<li class="fa fa-google-plus fa-1x" style="margin-right:20px;"></li>
				<li class="fa fa-linkedin-square fa-1x " style="margin-right:20px;"></li>
				<li  class="fa fa-twitter fa-1x "></li>
			</ul>
		</div>	 -->
		<div class="container text-center">
			<div class=" h_contact">
				<p class=" heads">Get In Touch With Us</p>
				<h5 class="hd">Fill Out The Form and we will get back to you asap.</h5>
			</div>
			<form id="frmContact" method="post" action="<?php echo base_url();?>Admin/send_message">
				<div class="text-center">
					<div class="relative"><input type="text" name="name" placeholder="Name*" class="inpt" ></div>
					<div class="relative"><input type="email" name="email" placeholder="Email*" class="inpt"></div>
					<div class="relative"><input type="text" name="subject" placeholder="Subject" class="inpt"></div>
					<div class="relative"><textarea type="text" name="message"  rows="3" placeholder="Your Message" class="texta"></textarea></div>
					<div class="btn">
            <button type="submit" name="button" class="btn send">Submit</button>
          </div>
				</div>
			</form>
			<ul class="text-center frw">
				<li class="fa fa-envelope fa-lg " style="margin-right:50px;"><span class="contact">edify.com</span></li>
				<li class="fa fa-phone fa-lg"><span class="contact">+914712527640</span></li>
			</ul>
			<ul class="text-center ffrw" >
				<li class="fa fa-facebook fa-1x frrw_st" style="margin-left:-50px;margin-right:25px;"></li>
				<li class="fa fa-google-plus fa-1x" style="margin-right:25px;"></li>
				<li class="fa fa-linkedin-square fa-1x " style="margin-right:25px;"></li>
				<li  class="fa fa-twitter fa-1x "></li>
			</ul>


		</div>

	</div>
</div>

	<div id="cdmModal" class="modal md-effect-1" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
		<div class="modal-content  colr2" id="wrap">
			<div class="modal-header" id="header">
				<h4 class="modal-title title">
				DATA DRIVEN CLINICAL RESEARCH
				</h4>
				<button type="button" id="modal_close" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="content">
				<div class="m_body text-justify">
					<p><?php echo $string;?></p>
					<div class="ems" style="border-top:1px solid #eeeeee;">
						<p><?php echo $str1;?></p>
						<div class="row">
							<div class="col-lg-6" >
								<p><?php echo $str2;?></p>
							</div>
							<div class="col-lg-6">
								<p><?php echo $str3;?></p>
							</div>
						</div>
					</div>
					<p>
						<?php echo $str4;?>
					</p>
					<div class="deep2" style="padding-top:8px;">
						<p>
						<?php echo $str5;?>
						</p>
					</div>
					<p>
					<?php echo $str6;?>
					</p>
					<p>
					<?php echo $str7;?>
					</p>
					<div class="deep2" style="padding-top:8px;">
					<p>
					<?php echo $str8;?>
					</p>
					</div>
					<p>
					<?php echo $str9;?>
					</p>
					<div class="ems">
  					<div class="deep2" style="padding-top:8px;">
  					<p>
  					       <?php echo $str10;?>
  					</p>
  					</div>
  					<p>
  					       <?php echo $str11;?>
  					</p>
					</div>
					<div class="deep2" style="padding-top:8px;">
					<p>
					<?php echo $str14;?>
					</p>
					</div>
					<p>
					<?php echo $str15;?>
					</p>
					<div class="deep2" style="padding-top:8px;">
					<p>
					<?php echo $str12;?>
					</p>
					</div>
					<p>
					<?php echo $str13;?>
					</p>
				</div>
			</div>
		</div>
		</div>
	</div>
	<div id="dsModal" class="modal md-effect-1" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
		<div class="modal-content  colr2" id="wrap">
			<div class="modal-header" id="header">
				<h4 class="modal-title title">
				DATA SCIENCE AS A SERVICE
				</h4>
				<button type="button" id="modal_close" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="content">
				<div class="m_body text-justify ">

						<p>
						<?php echo $data;?>
						</p>
						<div class="" style="border-top:1px solid #eeeeee;">
						<p>
						<?php echo $data1;?>
						</p>
						</div>
						<div class="bdata2" style="padding-top:5px;">
						<p>
						<?php echo $data2;?></p>


						</div>
						<div class="bdata">
							<p>
								<?php echo $data3;?></p>

						</div>
						<div class="bdata2" style="padding-top:5px;">
								<p>
								<?php echo $data4;?></p>


						</div>
						<div class="">
							<p>
								<?php echo $data5;?></p>

						</div>
						<div class="bdata2" style="padding-top:0.5px;">
								<p>
								<?php echo $data6;?></p>


						</div>
						<div class="bdata">
							<p>
								<?php echo $data7;?></p>

						</div>
						<div class="bdata2" style="padding-top:5px;">
								<p>
								<?php echo $data8;?></p>


						</div>
						<div class="">
							<p>
								<?php echo $data9;?></p>

						</div>
						<div class="bdata2" style="padding-top:5px;">
								<p>
								<?php echo $data10;?></p>


						</div>
						<div class="bdata">
							<p>
								<?php echo $data11;?></p>

						</div>


				</div>


			</div>
		</div>
		</div>
	</div>
	<!-- dsModal -->

	<div id="dsModal" class="modal md-effect-1" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
		<div class="modal-content  colr2" id="wrap">
			<div class="modal-header" id="header">
				<h4 class="modal-title title">
				DATA SCIENCE AS A SERVICE
				</h4>
				<button type="button" id="modal_close" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="content">
				<div class="m_body text-justify ">

						<p>
						<?php echo $data;?>
						</p>
						<div class="" style="border-top:1px solid #eeeeee;">
						<p>
						<?php echo $data1;?>
						</p>
						</div>
						<div class="bdata2" style="padding-top:5px;">
						<p>
						<?php echo $data2;?></p>


						</div>
						<div class="bdata">
							<p>
								<?php echo $data3;?></p>

						</div>
						<div class="bdata2" style="padding-top:5px;">
								<p>
								<?php echo $data4;?></p>


						</div>
						<div class="">
							<p>
								<?php echo $data5;?></p>

						</div>
						<div class="bdata2" style="padding-top:0.5px;">
								<p>
								<?php echo $data6;?></p>


						</div>
						<div class="bdata">
							<p>
								<?php echo $data7;?></p>

						</div>
						<div class="bdata2" style="padding-top:5px;">
								<p>
								<?php echo $data8;?></p>


						</div>
						<div class="">
							<p>
								<?php echo $data9;?></p>

						</div>
						<div class="bdata2" style="padding-top:5px;">
								<p>
								<?php echo $data10;?></p>


						</div>
						<div class="bdata">
							<p>
								<?php echo $data11;?></p>

						</div>


				</div>


			</div>
		</div>
		</div>
	</div>
	<!-- bdModal -->
	<div id="bdModal" class="modal md-effect-1" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
		<div class="modal-content  colr2" id="wrap">
			<div class="modal-header" id="header">
				<h4 class="modal-title title">
				BIG DATA ANALYTICS
				</h4>
				<button type="button" id="modal_close" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="content">
				<div class="m_body text-justify ">

						<p>
						<?php echo $bdata;?>
						</p>
						<div class="bdata2" style="padding-top:5px;">
						<p>
						<?php echo $bdata1;?>
						</p>
						</div>
						<div class="bdata">
							<p>
								<?php echo $bdata2;?></p>
							<p>
								<?php echo $bdata3;?></p>
						</div>
						<div class="bdata2" style="padding-top:5px;">
								<p>
								<?php echo $bdata4;?></p>


						</div>
						<div class="deep">
							<p>
								<?php echo $bdata5;?></p>

						</div>

				</div>


			</div>
		</div>
		</div>
	</div>
		<!-- pmModal -->
		<div id="pmModal" class="modal md-effect-1" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
			<div class="modal-content  colr2" id="wrap">
				<div class="modal-header" id="header">
					<h4 class="modal-title title">
					PREDICTIVE MODELLING
					</h4>
					<button type="button" id="modal_close"class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="content">
					<div class="m_body text-justify ">

							<p>
							<?php echo $predictive;?>
							</p>
							<p>
							<?php echo $predictive3;?>
							</p>
							<p>
							<?php echo $predictive4;?>
							</p>
							<div style="border-bottom:1px solid #eeeeee;">
							<p>
							<?php echo $predictive5;?>
							</p>
							</div>
							<div class="deep2" >
							<p style="padding-top:4px;">
							<?php echo $predictive1;?>
							</p>
							</div>
							<div class="predictive">
							<p >
							<?php echo $predictive2;?>
							</p>
							</div>


					</div>


				</div>
			</div>
			</div>
		</div>
	<!-- mlModal -->
	<div id="mlModal" class="modal md-effect-1" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content  colr2" id="wrap">
				<div class="modal-header" id="header">
					<h4 class="modal-title title">
						MACHINE LEARNING
					</h4>
					<button type="button" id="modal_close" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="content">
				<div class="m_body text-justify ">

					<p>
					<?php echo $machine;?>
					</p>
					<div class="machine">
					<p>
					<?php echo $machine1;?>
					</p>
					<div class="deep2">
					<p style="padding-top:10px;">
							<?php echo $machine2;?></p>
							</div>
					<p style="padding-top:8px;">
							<?php echo $machine3;?></p>
					</div>
					<div class="deep2">

						<p style="padding-top:10px;">
							<?php echo $machine4;?></p>
					</div>
					<p>
					<?php echo $machine5;?>
					</p>
					<div class="deep2">

						<p style="padding-top:10px;">
							<?php echo $machine6;?></p>
					</div>
					<p>
					<?php echo $machine7;?>
					</p>
					<div class="deep2">

						<p style="padding-top:10px;">
							<?php echo $machine8;?></p>
					</div>
					<p>
					<?php echo $machine9;?>
					</p>
					<div class="deep2">

						<p style="padding-top:10px;">
							<?php echo $machine10;?></p>
					</div>
					<p>
					<?php echo $machine11;?>
					</p>

				</div>


				</div>
			</div>
		</div>
	</div>
	<!-- dlModal -->
	<div id="dlModal" class="modal md-effect-1" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
		<div class="modal-content  colr2" id="wrap">
			<div class="modal-header" id="header">
				<h4 class="modal-title title">
				DEEP LEARNING
				</h4>
				<button type="button" id="modal_close" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="content">
				<div class="m_body text-justify ">

						<p>
						<?php echo $deep1;?>
						</p>
						<div class="deep1">
						<p>
						<?php echo $deep2;?>
						</p>
						</div>
						<div class="deep">
							<p>
								<?php echo $deep3;?></p>
							<div class="deep2">
							<p style="padding-top:8px;">
								<?php echo $deep4;?></p>
							</div>
							<p>
								<?php echo $deep5;?></p>
						</div>

				</div>


			</div>
		</div>
		</div>
	</div>
	<!-- emsModal -->
	<div id="emsModal" class="modal md-effect-1" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content  colr2" id="wrap">
				<div class="modal-header" id="header">
					<h4 class="modal-title title">
					ENTERPRISE MOBILITY AND SOFTWARE
					</h4>
					<button type="button" id="modal_close" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="content">
					<div class="m_body text-justify ">

							<p>
							<?php echo $ems1;?>
							</p>
							<div class="ems">
							<p>
							<?php echo $ems2;?>
							</p>
							</div>

					</div>


				</div>
			</div>
		</div>
	</div>


<!-- js files -->
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<!-- <script src="<?php echo base_url();?>assets/js/imagesloaded.pkgd.min.js"></script> -->
	<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>

	<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" crossorigin="anonymous"></script>
	<script src="<?php echo base_url();?>assets/js/particles.min.js"></script>
	<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/anime.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.fullPage.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/scripts.js"></script>
	<script src="<?php echo base_url();?>assets/js/app.js"></script>
	<script src="<?php echo base_url();?>assets/js/scrollMonitor.js"></script>
	<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/vis.min.js"></script>
 </body>
</html>
