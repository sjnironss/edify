<div class="content">
<div class="container-fluid">
<div class="row text-center">
		<div class="col-md-2"></div>
        <div class="col-md-10 cont">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                  <div class="row">
                    <div class="col-md-12" style="text-align:center">
                    <h4 class="title">Messages</h4>
                    <p class="category">Complete with messages..</p></div>
                   
                       
                </div>
              </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                        <th>Sl.No</th>
                          <th>Name</th>
                          <th>Email</th>
                           <th>Subject</th>
                          <th>Message</th>
                            <th>Action</th>
                        </thead>
                        <tbody class="text-left">
                          <?php
                           $i=1;
                          foreach ($result as  $message) {?>
                            <tr>
                              <td><?=$i?></td>
                               <td><?=$message['name']?></td>
                               <td><?=$message['email']?></td>
                               <td><?php echo $message['subject'];?></td>
                               <td><?php echo $message['message'];?></td>
                                <td><a href="" data-toggle="modal" data-target="#myModal<?=$message['message_id'] ?>">Delete</a></td>
                             </tr>
                               <div id="myModal<?=$message['message_id']?>" class="modal fade" role="dialog" style="z-index:10;">
                                 <div class="modal-dialog">
                                 <div class="modal-content">
                                 <div class="modal-header">
                                 <div class="modal-title">
                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h5>Do you want to delete ?</h5>
                                 </div>
                                 <div class="modal-footer">
                                 <form method="post" action="<?=base_url();?>admin/delete_message/<?=$message['message_id']?>">
                                     <button class="btn btn-default">Yes</button>
                                  </form>
                                   <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                   </div>
                                   </div>
                                   </div>
                                 </div>
                               </div>

                                <?php $i++; }?>


                        </tbody>
                    </table>

                </div>
            </div>
        </div>
