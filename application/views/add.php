


<div class="content">
<div class="container-fluid" >
	<div class="row text-center">
		<div class="col-md-2"></div>
		<div class="col-md-10 cont" style="text-align:center;">
			<div class="card">
				<div class="card-header" data-background-color="purple">
					<h4 class="title">ADD PORTFOLIO / BLOG</h4>
					<p class="category">Complete with Portfolio and Blog</p>
				</div>
				<div class="card-content">
					<form method="post" action="<?=base_url();?>Admin/add_details" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Short Heading</label>
									<input type="text" class="form-control" name="sheading" required >
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Long Heading</label>
									<input type="text" class="form-control" name="lheading" required >
								</div>
							</div>
						</div>

						<div class="row">

							<div class="col-md-6">
								<div class="form-group label-floating">
									<!-- <label class="control-label">Select Type</label> -->
									<!-- <input type="text" class="form-control" > -->
									<select class="form-control" name="type" required>
										<option value="">Select...</option>
									  <option value="portfolio">Portfolio</option>
									  <option value="blog">Blog</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group ">
									
									<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Upload Image</button>
										
									<!-- <input type="file" class="form-control " name="style="opacity:.6"content_image"   id="fileUpload" data-width="600" data-height="400"> -->
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
							 <div class="form-group label-floating">
								 <label class="control-label">Short Description</label>
								 <input type="text" class="form-control" name="shortd" required>
							 </div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
							 <div class="form-group label-floating">
								 <label class="control-label">Long Description</label>
								 <textarea id="longd" name="longd">long description here..</textarea>
							 </div>
							</div>
						</div>

						<button type="submit" id="upload" class="btn btn-primary pull-right" onclick="return Upload()">Submit</button>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
</div>

<div class="modal fade" id="myModal" role="dialog" aria-hidden="false"	>
	<div class="modal-dialog">

  	 	<div class="modal-content">
			<div class="modal-header">
	  			<button type="button" class="close" data-dismiss="modal">&times;</button>
	 			<h4 class="modal-title">Upload Image </h4>
			</div>
			<div class="modal-body">
				<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data">
	  				<p><input name="image" type="file" value="image" style="opacity:.6" /></p>
	  				<input type="submit" name="upload" value="Upload" />
	  			</form>
			</div>
	
			<div class="modal-footer">
			`	<button type="submit" class="btn btn-default" >Ok</button>
	  	 		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
  	</div>
</div>
<?php
    $folder='uploads/images/';
    $orig_w=500;
    if(isset($_POST['upload']) ) {
		$imageFile=$_FILES['image']['tmp_name'];
        $filename=basename($_FILES['image']['name']);
        list($width,$height)=getimagesize($imageFile);
        $src=imagecreatefromjpeg($imageFile);
        $orig_h=($height/$width)*$orig_w;
        $tmp=imagecreatetruecolor($orig_w,$orig_h);
        imagecopyresampled($tmp,$src,0,0,0,0,$orig_w,$orig_h,$width,$height);
        imagejpeg($tmp,$folder.$filename,100);
        imagedestroy($tmp);
        imagedestroy($src);
        $filename=urlencode($filename);
		header("Location:crop?filename=$filename&height=$orig_h");
	 //echo $filename;die();
       }
?>

