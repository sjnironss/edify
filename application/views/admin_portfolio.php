
<div class="content">
<div class="container-fluid" >
	<div class="row text-center">
		<div class="col-md-2"></div>
		<div class="col-md-10 cont" style="text-align:center;">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                  <div class="row">
                    <div class="col-md-9" style="text-align:center">
                    <h4 class="title">PORTFOLIO </h4>
                    <p class="category">Latest Portfolioes...</p></div>
                    <div class="col-md-3">
                        <form method="post" action="<?=base_url();?>Admin/add_portfolio">
                            <button type="submit" class="btn btn-lg" style="background: #bf5ed6;">ADD PORTFOLIO</button>
                        </form>
                    </div>
                    </div>

                </div> 
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                           <th>Sl.No</th>
                          <th>Heading</th>
                          <th>Long Heading</th>
                         
                          <th>Image</th>
                          <th>Description</th>
                          <th colspan="2">Actions</th>
                          
                         
                      
                        </thead>
                        <tbody>
                          <?php
                          $i=1;

            foreach ($result as $content) {?>

              <tr>
                  <td><?=$i?></td>
                  <td><?=$content['heading']?></td>

                  <td><?=$content['long_heading']?></td>
                 
                 
                  
                  <td>  <img src="<?php echo base_url().'/uploads/images/'.$content['image'];?>" height="128" width="128">
                 </td>
                  <td><?=$content['description']?></td>
                 
                  
                 
                  <td><a href="" data-toggle="modal" data-target="#myModal<?=$content['portfolio_id'] ?>">Delete</a></td>
                  
                   
                       <div id="myModal<?=$content['portfolio_id'] ?>" class="modal fade" role="dialog" style="z-index:10;">
                         <div class="modal-dialog">
                           <div class="modal-content">
                             <div class="modal-header">
                               <div class="modal-title">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h5>Do you want to delete ?</h5>
                                 </div>
                                 <div class="modal-footer">
                       <form method="post" action="<?=base_url();?>Admin/delete_portfolio/<?=$content['portfolio_id']?>">
                                               <button  class="btn btn-default" >
                                                   Yes</button>  </form>
                                               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                             
                                             </div>
                                           </div>
                                         </div></div>
                                         </div>
</div>
  <td><a href="" data-toggle="modal" data-target="#myModal2<?=$content['portfolio_id'] ?>">Update</a></td>
                  
                   </tr>
                       <div id="myModal2<?=$content['portfolio_id'] ?>" class="modal fade" role="dialog" style="z-index:10;">
                         <div class="modal-dialog">
                           <div class="modal-content">
                             <div class="modal-header">
                               <div class="modal-title">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h5>Do you want to update?</h5>
                                 </div>
                                 <div class="modal-footer">
                       <form method="post" action="<?=base_url();?>Admin/update_portfolio/<?=$content['portfolio_id']?>">
                                               <button  class="btn btn-default" >
                                                   Yes</button>  </form>
                                               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                             </div>
                                           </div>
                                         </div></div>
                                         </div>



             <?php $i++; }?>

                        </tbody>
                    </table>
                    </div>
                  </div>
                </div>
            </div>
        </div>


    </div>
