
<div class="content">
<div class="container-fluid" >
	<div class="row text-center">
		<div class="col-md-2"></div>
		<div class="col-md-10 cont" style="text-align:center;">
			<div class="card">
				<div class="card-header" data-background-color="purple">
					<h4 class="title">EDIT PORTFOLIO / BLOG</h4>
					<p class="category">Complete with Portfolio and Blog</p>
				</div>
				<?php foreach ($result as $content) {?>
				<div class="card-content">
					<form method="post" action="<?=base_url();?>Admin/new_update/<?=$content['id']?>" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Short Heading</label>
									<input type="text" class="form-control" name="sheading" value="<?=$content['short_heading']?>" required >
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Long Heading</label>
									<input type="text" class="form-control" name="lheading" value="<?=$content['long_heading']?>" required >
								</div>
							</div>
						</div>

						<div class="row">

							<div class="col-md-6">
								<div class="form-group label-floating">
									<!-- <label class="control-label">Select Type</label> -->
									<!-- <input type="text" class="form-control" > -->
									<select class="form-control" name="type" value="<?=$content['type']?>" required>
										<option value="<?=$content['type']?>"><?=$content['type']?></option>
									  <option value="portfolio">Portfolio</option>
									  <option value="blog">Blog</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group ">
									<label class=""></label>
									<input type="file" class="form-control" name="content_image" value="<?$content['content_image']?>" style="opacity:.6" >
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
							 <div class="form-group label-floating">
								 <label class="control-label">Short Description</label>
								 <input type="text" class="form-control" name="shortd" value="<?=$content['short_desc']?>" required>
							 </div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
							 <div class="form-group label-floating">
								 <label class="control-label">Long Description</label>
								 <textarea id="longd"name="longd"> <?=$content['long_description']?></textarea>
							 </div>
							</div>
						</div>

						<button type="submit" class="btn btn-primary pull-right">Submit</button>
						<div class="clearfix"></div>
					<?php	}?>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
</div>
