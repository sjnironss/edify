<?php
    $folder='uploads/images/';
    $filename=$_GET['filename'];
    $orig_w=500;
    $orig_h=$_GET['height'];
    $targ_w=120;
    $targ_h=100;
    $ratio=   $targ_w/$targ_h;
    if(isset($_POST['submit'])){
        $src=imagecreatefromjpeg($folder.$filename); 
        $tmp=imagecreatetruecolor($targ_w,$targ_h);
        imagecopyresampled($tmp,$src,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);
        imagejpeg($tmp,$folder.'t_'.$filename,100);
        imagedestroy($tmp); 
        imagedestroy($src); 
        // echo " <h1>Saved Thumbnail</h1><img src=\" $folder/t_$filename\"/>;
    }

?>
<html>
<head>
<script src="../../assets/js/jquery.min.js"></script>

<link rel="stylesheet" href="../../assets/css/Jcrop.min.css" type="text/css">
<script type="text/javascript">
$(document).ready(function(){
    console.log("ready")
    $('#cropbox').Jcrop({
        aspectRatio: 1,
        setSelect: [ 175, 100, 400, 300 ],
        boxWidth: 400,   //Maximum width you want for your bigger images
        boxHeight: 400,
        onSelect: updateCoords 
    },function(){
  var jcrop_api = this;
  thumbnail = this.initComponent('Thumbnailer', { width: 130, height: 130 });
});
   
});
function updateCoords(c){
    showPreview(c);
    $("#x").val(c.x);
    $("#y").val(c.y);
    $("#w").val(c.w);
    $("#h").val(c.h);

}
function showPreview(coords)
{
    var rx=<?php echo $targ_w?>/coords.w;
    var ry=<?php echo $targ_h?>/coords.h;
    $("#preview img").css({
        width:Math.round(rx*<?php echo $orig_w;?>)+'px',
        height:Math.round(ry*<?php echo $orig_h;?>)+'px',
        marginLeft:'-'+Math.round(rx*coords.x)+'px',
        marginRight:'-'+Math.round(ry*coords.y)+'px'

    });
}
</script>
    <style type="text/css">
        /* #preview{
            width:<?php echo $targ_h?>px;
            height:<?php echo $targ_w?>px;
            overflow:hidden;
        } */
        #cropbox{
            width:400px;
            height:400px;
        }
        .jcrop-thumb {
  top: 15px;
  right: -80px;
  border: 1px black solid;
}
        /* .cropbox-holder{
            width:400px;
            height:400px;
            overflow:hidden;
        } */
    </style>
</head>
<body>
    <table>
        <tr>
        <td>
        <!-- <div class="cropbox-holder">
            <img src="../../uploads/images/001.jpg" id="cropbox" alt="cropbox"/>
        </div> -->
            <img src="../../<?php echo $folder.$filename?>" id="cropbox" alt="cropbox"/>
        </td>
        
        </tr>
    </table>
    <form action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">
        <p>
            <input type="hidden" id="x" name="x"/>
            <input type="hidden" id="y" name="y"/>
            <input type="hidden" id="w" name="w"/>
            <input type="hidden" id="h" name="h"/>
            <input type="submit" id="submit" name="submit" value="crop Image"/>
        </p>
    </form>
    <script src="../../assets/js/Jcrop.min.js"></script>
</body>
</html>