
<div class="content">
<div class="container-fluid" >
	<div class="row text-center">
		<div class="col-md-2"></div>
		<div class="col-md-10 cont" style="text-align:center;">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                  <div class="row">
                    <div class="col-md-10" style="text-align:center">
                    <h4 class="title">BLOGS </h4>
                    <p class="category">Latest blogs...</p></div>
                    <div class="col-md-2">
                        <form method="post" action="<?=base_url();?>Admin/add_blogs">
                            <button type="submit" class="btn btn-lg" style="background: #bf5ed6;">ADD BLOG</button>
                        </form>
                    </div>
                    </div>

                </div> 
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                           <th>Sl.No</th>
                          <th>Name</th>
                          <th>Designation</th>
                         
                          <th>Image</th>
                          <th>Description</th>
                          <th colspan="2">Actions</th>
                          
                         
                      
                        </thead>
                        <tbody>
                          <?php
                          $i=1;

            foreach ($result as $content) {?>

              <tr>
                  <td><?=$i?></td>
                  <td><?=$content['name']?></td>

                  <td><?=$content['designation']?></td>
                 
                 
                  
                  <td>  <img src="<?php echo base_url().'/uploads/images/'.$content['image'];?>" height="128" width="128">
                 </td>
                  <td><?=$content['description']?></td>
                 
                  
                 
                  <td><a href="" data-toggle="modal" data-target="#myModal<?=$content['blog_id'] ?>">Delete</a></td>
                  
                   
                       <div id="myModal<?=$content['blog_id'] ?>" class="modal fade" role="dialog" style="z-index:10;">
                         <div class="modal-dialog">
                           <div class="modal-content">
                             <div class="modal-header">
                               <div class="modal-title">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h5>Do you want to delete ?</h5>
                                 </div>
                                 <div class="modal-footer">
                       <form method="post" action="<?=base_url();?>Admin/delete_blog/<?=$content['blog_id']?>">
                                               <button  class="btn btn-default" >
                                                   Yes</button>  </form>
                                               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                             
                                             </div>
                                           </div>
                                         </div></div>
                                         </div>
</div>
  <td><a href="" data-toggle="modal" data-target="#myModal2<?=$content['blog_id'] ?>">Update</a></td>
                  
                   </tr>
                       <div id="myModal2<?=$content['blog_id'] ?>" class="modal fade" role="dialog" style="z-index:10;">
                         <div class="modal-dialog">
                           <div class="modal-content">
                             <div class="modal-header">
                               <div class="modal-title">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h5>Do you want to update?</h5>
                                 </div>
                                 <div class="modal-footer">
                       <form method="post" action="<?=base_url();?>Admin/update_blog/<?=$content['blog_id']?>">
                                               <button  class="btn btn-default" >
                                                   Yes</button>  </form>
                                               <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                             </div>
                                           </div>
                                         </div></div>
                                         </div>



             <?php $i++; }?>

                        </tbody>
                    </table>
                    </div>
                  </div>
                </div>
            </div>
        </div>


    </div>
