<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->model("Admin_model");
		$portfolios = $this->Admin_model->view_portfolio();
		$data["portfolio"] = [];
		foreach($portfolios as $portfolio){
			$tmp['id'] = $portfolio['portfolio_id'];
			$tmp['label'] = $portfolio['heading'];
			$tmp['long_head']=$portfolio['long_heading'];
			$tmp['image']=$portfolio['image'];
			$tmp['description']=$portfolio['description'];
			array_push($data["portfolio"], $tmp);
		}
		$data['blog']=$this->Admin_model->view_blog();
		// print_r($data["portfolio"]); die();
		//$this->load->view('home');
		//$data_json = json_encode($data);
		$this->load->view('home',$data);
	}

	
}
?>