<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
	   parent:: __construct();
	   $this->baseurl_admin = base_url().'assets/admin/';
	   
	   $this->load->model("Admin_model");
	   $this->load->helper('cookie');
	//    $this->load->library('session');
	 
	}

	public function index()
	{
		$this->load->view('adminlogin');
		//$this->load->view('upload_blog');
	}
	public function upload()
	{
	 $this->load->view('upload');
	}
    public function crop()
	{
		$this->load->view('crop');
	}
    public function login()
	{


			$this->load->library('form_validation');

			$this->form_validation->set_rules('username', 'username', 'required');
			$this->form_validation->set_rules('password', 'password', 'required');

			if ($this->form_validation->run() == FALSE) {
				$this->index();
				}
				else
				{
					$user =$this->input->post("username");
					$pass =$this->input->post("password");
                    $this->load->model("Admin_model");
					$userdata=$this->Admin_model->login($user,$pass);
					if($userdata == TRUE)
					{
                       
						 $d['res']=$this->Admin_model->login($user,$pass);
						$id=$d['res'][0]['id'];
                       $_SESSION['user'] = $data['user'];
						if(!empty($_POST["remember_me"])) {
							set_cookie("uid",$id,'259200');
							set_cookie("uname",$user,'259200');
							set_cookie("password",$pass,'259200');

		 					//echo get_cookie("uname");
							//echo $_COOKIE['uname'];die();
		 					//echo get_cookie("password");

						} else {
							set_cookie("uid",$id,'6200');
							//set_cookie("type",$type,'6200');
							set_cookie("uname",$user,'6200');
							set_cookie("password",$pass,'6200');
						}
                      
                    // echo $_COOKIE["uid"];
						redirect('admin/view_message');
					}
				else
				{
				$this->index();
				}


	}

}
	// Public function adminview()
	// {
	// 	$this->load->view('includes/header');
		
	// 	$this->load->view('admin_view');
	// 	$this->load->view('includes/footer');

			
	// }
	public function changepassword()
	{
		$this->load->view('includes/header');
		
		$this->load->view('changepassword');
		$this->load->view('includes/footer');
		
		
	}
	public function addnew()
	{
		  $this->load->view('includes/header');
			$this->load->view('add');
		 $this->load->view('includes/footer');	
	
	}
	public function add_blogs()
	{
		  $this->load->view('includes/header');
			$this->load->view('add_blog');
		 $this->load->view('includes/footer');	
	
	}
	public function add_blog_data()
	{
		
		if(!empty($_FILES['image']['name'])){
			$config['upload_path'] = 'uploads/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['image']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('image')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];
			}else{
				$picture = 'photo.gif';
			}
		}else{
			$picture = 'photo.gif';
		}
		
	$data =array(
		'name'	=>$this->input->post('name'),
		'designation'	=>$this->input->post('designation'),
		'image' =>$picture,
		'description'	=>$this->input->post('desc'),
		
	);

		
		$res=$this->Admin_model->add_blog($data);
		if($res==true)
		{
			redirect('admin/view_blog');
		}

	}
	public function view_blog()
	{
		
		$this->load->view('includes/header');
		$data["result"]=$this->Admin_model->get_blog();
		$this->load->view('admin_blog',$data);
		$this->load->view('includes/footer');
	}
	public function delete_blog($id)
	{
	 
	   $this->Admin_model->delete_blog($id);
		$this->load->view('includes/header');
	   redirect('Admin/view_blog');
		$this->load->view('includes/footer');
   }
   public function update_blog($id)
   {
	   
	   $data["result"]=$this->Admin_model->edit_blog($id);
	   $this->load->view('includes/header');
	   $this->load->view('edit_blog',$data);
	   $this->load->view('includes/footer');
   }
   public function blog_update($id)
   
	{
		if(!empty($_FILES['image']['name'])){
			$config['upload_path'] = 'uploads/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['image']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('image')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];
			}else{
				$picture = 'photo.gif';
			}
		}else{
			$picture = 'photo.gif';
		}
		
	$data =array(
		'name'	=>$this->input->post('name'),
		'designation'	=>$this->input->post('designation'),
		'image' =>$picture,
		'description'	=>$this->input->post('desc'),
	);

		
		$res=$this->Admin_model->blog_update($id,$data);
		if($res==true)
		{
			redirect('admin/view_blog');
		}

	}
	public function add_portfolio()
	{
		  $this->load->view('includes/header');
			$this->load->view('add_portfolio');
		 $this->load->view('includes/footer');	
	
	}
	public function add_portfolio_data()
	{
		
		if(!empty($_FILES['image']['name'])){
			$config['upload_path'] = 'uploads/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['image']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('image')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];
			}else{
				$picture = 'photo.gif';
			}
		}else{
			$picture = 'photo.gif';
		}
		
	$data =array(
		'heading'	=>$this->input->post('heading'),
		'long_heading'	=>$this->input->post('long_head'),
		'image' =>$picture,
		'description'	=>$this->input->post('desc'),
		
	);

		
		$res=$this->Admin_model->add_portfolio($data);
		if($res==true)
		{
			redirect('admin/view_portfolio');
		}

	}
	public function delete_portfolio($id)
	{
	 
	   $this->Admin_model->delete_portfolio($id);
		$this->load->view('includes/header');
	   redirect('Admin/view_portfolio');
		$this->load->view('includes/footer');
   }
	public function view_portfolio()
	{
		
		$this->load->view('includes/header');
		$data["result"]=$this->Admin_model->get_portfolio();
		$this->load->view('admin_portfolio',$data);
		$this->load->view('includes/footer');
	}
	public function update_portfolio($id)
	{
		
		$data["result"]=$this->Admin_model->edit_portfolio($id);
		$this->load->view('includes/header');
		$this->load->view('edit_portfolio',$data);
		$this->load->view('includes/footer');
	}
	public function portfolio_update($id)
	
	 {
		 if(!empty($_FILES['image']['name'])){
			 $config['upload_path'] = 'uploads/images/';
			 $config['allowed_types'] = 'jpg|jpeg|png|gif';
			 $config['file_name'] = $_FILES['image']['name'];
 
			 //Load upload library and initialize configuration
			 $this->load->library('upload',$config);
			 $this->upload->initialize($config);
 
			 if($this->upload->do_upload('image')){
				 $uploadData = $this->upload->data();
				 $picture = $uploadData['file_name'];
			 }else{
				 $picture = 'photo.gif';
			 }
		 }else{
			 $picture = 'photo.gif';
		 }
		 
	 $data =array(
		 'heading'	=>$this->input->post('heading'),
		 'long_heading'	=>$this->input->post('long_head'),
		 'image' =>$picture,
		 'description'	=>$this->input->post('desc'),
	 );
 
		 
		 $res=$this->Admin_model->portfolio_update($id,$data);
		 if($res==true)
		 {
			 redirect('admin/view_portfolio');
		 }
 
	 }

	public function reset_password()
	{

		$pass = array(
					// $id = $uid,
                  'password' => $this->input->post('password')
                 );
		
		$this->Admin_model->reset_password($pass);
		
		redirect('admin/view_content');
	}
	public function send_message(){
		$data =array(
			'name'	=>$this->input->post('name'),
			'email'	=>$this->input->post('email'),
			'subject'	=>$this->input->post('subject'),
			'message'	=>$this->input->post('message'),
		);
	
			
			$res=$this->Admin_model->send_message($data);
			if($res==true)
			{
				redirect('welcome');
			}
	}
	public function view_message(){
		$this->load->view('includes/header');
		$data["result"]=$this->Admin_model->get_message();
		$this->load->view('message_view',$data);
		$this->load->view('includes/footer');
	}
	public function delete_message($id)
	{
	 
	   $this->Admin_model->delete_message($id);
		$this->load->view('includes/header');
	   redirect('Admin/view_message');
		$this->load->view('includes/footer');
   }
	public function logout() {
		
				delete_cookie("uid");
				delete_cookie("uname");
				//delete_cookie("password");
				//setcookie("uname", "", time()- 60, "/","", 0);
				 //setcookie("password", "", time()- 60, "/","", 0);
				redirect('admin/index');
			  }
		
	public function add_details()
	{
		
		if(!empty($_FILES['content_image']['name'])){
			$config['upload_path'] = 'uploads/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['content_image']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('content_image')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];
			}else{
				$picture = 'photo.gif';
			}
		}else{
			$picture = 'photo.gif';
		}
		
	$data =array(
		'short_heading'	=>$this->input->post('sheading'),
		'long_heading'	=>$this->input->post('lheading'),
		'type'			=>$this->input->post('type'),
		'content_image' =>$picture,
		'short_desc'	=>$this->input->post('shortd'),
		'long_description'	=>$this->input->post('longd')
	);

		
		$res=$this->Admin_model->add_content($data);
		if($res==true)
		{
			redirect('admin/view_content');
		}

	}


	public function view_content()
	{
		
		$this->load->view('includes/header');
		$data["result"]=$this->Admin_model->get_content();
		$this->load->view('admin_view',$data);
		$this->load->view('includes/footer');
	}
	public function delete_content($id)
	{
	 
	   $this->Admin_model->delete_content($id);
		$this->load->view('includes/header');
	   redirect('Admin/view_content');
		$this->load->view('includes/footer');
   }
   public function update_content($id)
   {
	   
	   $data["result"]=$this->Admin_model->edit_content($id);
	   $this->load->view('includes/header');
	   $this->load->view('editcontent',$data);
	   $this->load->view('includes/footer');
   }


   public function new_update($id)
   
	{
		if(!empty($_FILES['content_image']['name'])){
			$config['upload_path'] = 'uploads/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['content_image']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('content_image')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];
			}else{
				$picture = 'photo.gif';
			}
		}else{
			$picture = 'photo.gif';
		}
		
	$data =array(
		'short_heading'	=>$this->input->post('sheading'),
		'long_heading'	=>$this->input->post('lheading'),
		'type'			=>$this->input->post('type'),
		'content_image' =>$picture,
		'short_desc'	=>$this->input->post('shortd'),
		'long_description'	=>$this->input->post('longd')
	);

		
		$res=$this->Admin_model->new_update($id,$data);
		if($res==true)
		{
			redirect('admin/view_content');
		}

	}
   
   
}?>