<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Model extends CI_Model {

	function __construct(){
	parent::__construct();
	}

	function login($user,$pass)
	{
		$this->db->select("*");
		$this->db->from("login");
		$this->db->where("username",$user);
		$this->db->where("password",$pass);
		$query=  $this->db->get();
		return $query->result_array();
		if ($query->num_rows() > 0){

	        return TRUE;
	    }
	    else
	    {
	    	return FALSE;
	    }
	}
	
		public function reset_password($pass)
		{
	
		$this->db->where('id', $_COOKIE['uid']);
		$this->db->update('login',$pass);
	
		}
		public function add_content($data)
		{
			$this->db->INSERT("content",$data);
			return true;
		}
		public function add_blog($data)
		{
			$this->db->INSERT("blogs_table",$data);
			return true;
		}
		public function get_blog()
		{
			$this->db->select("*");
				$this->db->from("blogs_table");
			
				$query=  $this->db->get();
				return $query->result_array();
		
		}
		public function delete_blog($id)
		{
			$this->db->where(['blog_id'=>$id]);
			$this->db->delete("blogs_table");
		
		}
		public function edit_blog($id)
		{
			$this->db->select("*");
			$this->db->from("blogs_table");
			$this->db->where(['blog_id'=>$id]);
			$query=  $this->db->get();
			return $query->result_array();
		
		}
		public function blog_update($id,$data)
		{
			$this->db->where(['blog_id'=>$id]);
			$this->db->update("blogs_table",$data);
		
			return true;
		
		}
		public function add_portfolio($data)
		{
			$this->db->INSERT("portfolio_table",$data);
			return true;
		}
		public function delete_portfolio($id)
		{
			$this->db->where(['portfolio_id'=>$id]);
			$this->db->delete("portfolio_table");
		
		}
		public function get_portfolio()
		{
			$this->db->select("*");
				$this->db->from("portfolio_table");
			
				$query=  $this->db->get();
				return $query->result_array();
		
		}
		public function edit_portfolio($id)
		{
			$this->db->select("*");
			$this->db->from("portfolio_table");
			$this->db->where(['portfolio_id'=>$id]);
			$query=  $this->db->get();
			return $query->result_array();
		
		}
		public function portfolio_update($id,$data)
		{
			$this->db->where(['portfolio_id'=>$id]);
			$this->db->update("portfolio_table",$data);
		
			return true;
		
		}
		public function get_content()
		{
			$this->db->select("*");
				$this->db->from("content");
			
				$query=  $this->db->get();
				return $query->result_array();
		
		}
		public function delete_content($id)
		{
			$this->db->where(['id'=>$id]);
			$this->db->Delete("content");
		
		}
		public function edit_content($id)
		{
			$this->db->select("*");
			$this->db->from("content");
			$this->db->where(['id'=>$id]);
			$query=  $this->db->get();
			return $query->result_array();
		
		}
		public function new_update($id,$data)
		{
			$this->db->where(['id'=>$id]);
			$this->db->Update("content",$data);
		
			return true;
		
		}
		public function view_portfolio()
		{
			$sql="SELECT * FROM portfolio_table ORDER BY date DESC ";
			$query = $this->db->query( $sql );
			return $query->result_array();
		}
		public function view_blog()
		{
			$sql="SELECT * FROM blogs_table ORDER BY date DESC ";
			$query = $this->db->query( $sql );
			return $query->result_array();
		}
		public function send_message($data)
		{
			$this->db->INSERT("message_table",$data);
			return true;
		}
		public function get_message()
		{
			$this->db->select("*");
				$this->db->from("message_table");
			
				$query=  $this->db->get();
				return $query->result_array();
		
		}
		public function delete_message($id)
		{
			$this->db->where(['message_id'=>$id]);
			$this->db->Delete("message_table");
		
		}

}?>