$(document).ready(function() {
	$("#fullpage").imagesLoaded(function() {
		$("#main_loader").hide();
	});
	$('.modal').on('shown.bs.modal', function() {
		$.fn.fullpage.setAllowScrolling(false);
		$.fn.fullpage.setKeyboardScrolling(false);
	});
	$('.modal').on('click', '#modal_close', function() {
		$.fn.fullpage.setAllowScrolling(true);
		$.fn.fullpage.setKeyboardScrolling(true);
	});

	$("#blog-owl").owlCarousel({
		autoplay: true,
		autoplayTimeout: 3000,
		autoplayHoverPause: true,
		// items : 3,
		// nav:true,
		center: true,
		//loop: true,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
				nav: false
			},
			425: {
				items: 1,
				nav: false,
				loop: true
			},
			667: {
				items: 1,
				nav: false,
				loop: true
			},
			768: {
				items: 3,
				nav: false,
				loop: true
			},
			1024: {
				items: 3,
				nav: false,
				loop: true
			},
			1440: {
				items: 3,
				nav: false,
				loop: true
			}
		}
	});

	$("#expertise-owl").owlCarousel({
		autoplay: true,
		autoplayTimeout: 4000,
		autoplayHoverPause: true,
		// nav:true,
		center: true,
		//loop: true,
		// items:3,
		navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			425: {
				items: 1,
				nav: true,
				loop: true
			},

			768: {
				items: 1,
				nav: true,
				loop: true
			},
			1024: {
				items: 3,
				nav: true,
				loop: true
			},
			1440: {
				items: 3,
				nav: true,
				loop: true
			}
		}
	});

	$("#frmContact").validate({
	  rules: {
	    name: {
	      required: true
	    },
			email: {
	      required: true,
	      email: true
	    },
			message:{
				required: true,
      	minlength: 10
			}
	  },
		messages:{
			message:{
      	minlength: "Message should contain atleast 10 characters"
			}
		}
	});

});
$('#fullpage').fullpage({
	css3: true,
	anchors: ['home', 'company', 'solutions', 'blog', 'portfolio', 'graph', 'contact'],
	menu: '#myMenu',
	navigation: false,
	navigationPosition: 'right',
	parallax: 'true',
	slidesNavigation: true,
	slidesNavPosition: 'bottom',
	scrollHorizontally: true,
	afterLoad: function(anchorLink, index) {
		//flipping numbers
		$('.counter').each(function() {
			$(this).html(0)
		});
		if (index == 6) {
			setTimeout(function() {
				console.log(index);
				$('.counter').each(function() {
					var $this = $(this),
						countTo = $this.attr('data-count');
					$({
						countNum: $this.text()
					}).animate({
						countNum: countTo
					}, {
						duration: 1200,
						easing: 'linear',
						step: function() {
							$this.text(Math.floor(this.countNum));
						},
						complete: function() {
							$this.text(this.countNum);
						}
					});
				});
			}, 200)
		}
		// portfolio bubbles
		if (index == 5) {

			var nodes = new vis.DataSet(portfolios);
			// console.log(portfolios);
			var edges = new vis.DataSet();
			var container = document.getElementById('bubbles');
			var data = {
				nodes: nodes,
				edges: edges
			};
			var options = {
				nodes: {
					borderWidth: 0,
					shape: "circle",
					color: {
						// background:'#fff',
						//background:'#138094',
						//background:'#03267e',
						//background:'#1c3a88',
						background: '#f92c55',


						highlight: {
							background: '#f92c55',
							border: '#f92c55'
						}
					},
					font: {
						// color:'#147890'
						color: '#fff'
					}

				},
				physics: {
					stabilization: false,
					minVelocity: 0.01,
					solver: "forceAtlas2Based",
					repulsion: {
						nodeDistance: 50
					}
				}
			};
			var network = new vis.Network(container, data, options);
			//Events
			network.on("click", function(e) {
				if (e.nodes.length) {
					var node = nodes.get(e.nodes[0]);
					console.log(node['label']);
					$("#networkModal #node").val(node['label']);
					$("#hi").html(node['label']);
					$("#head").html(node['long_head']);
					//  console.log("image", base_url+"uploads/images/"+node['image']);
					$("#modal-image").attr("src", base_url + "uploads/images/" + node['image']);
					$("#desc").html(node['description']);
					$("#networkModal").modal({
						show: true,
						closeOnEscape: true,
						node: node,
					});
					nodes.update(node);

				}
			});

		}
	}

});
